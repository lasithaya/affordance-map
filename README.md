#  Affordance Map : Learning Hidden Human Context in 3D Scenes Through Virtual Human Models



Ability to learn human context in an environment could be one of the most desired fundamental abilities that a robot should possess when sharing workspaces with human coworkers. Arguably, a robot with appropriate human context awareness could lead to a
better human robot interaction. This research  addresses the problem of learning human context in indoor environments by only looking at geometrics features of the environment. The novelty of this concept is, it does not require to observe real humans to learn
human context. Instead, it uses virtual human models and their relationships with the environment to map hidden human affordances in 3D scenes

![alt text](https://bitbucket.org/lasithaya/affordance-map/downloads/affordance-map.png "Logo Title Text 1")


### Matlab script description. Each folder corresponds to diiferent SVM algorithms


- Learnroom_main-** is the main learning function
- Inferroom_main is the main inference function


 These files need  mat files generated from  the follwing script files


- create_skeleteons : creates skeleton pcd files for each pose
- create2_Yground : calculates the Torso position of the groundtruth  ss1.ply. A skeleton file need to be manually placed and should be created
  for each data set.

- creat3_fullmat_rotation_translation : creates rotation and translation for 1000 features.

- Create4_SitN Leg : creates mat files for sitting and leg files to create  grounddtruth
- create5_disAllRooms : creates distance maps and occupancy maps for each room. Modify map_dis according to the requirement.


### Please refer following papers and the thesis for more details and cite them  if you use this codebase

 - https://ieeexplore.ieee.org/document/7419073
 - https://opus.lib.uts.edu.au/bitstream/10453/126029/1/camera%20ready.pdf
 - https://opus.lib.uts.edu.au/bitstream/10453/43499/2/02whole.pdf
  
 
 



