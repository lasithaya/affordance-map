clear
skel_file =cellstr(['relaxing';'leaning ';'working ']);


format long;

room_num =[1 8 7];
% learning dataset for each pose. select the room numer for each pose. 1= relaxing, 2=leaning,3= working
room_array{1}=[1,2,11];
room_array{2}=[5,6,8,9,10];
room_array{3}=[3,4,5,6,7];


%  poses 1= relaxing, 2=leaning,3= working

for pose= 1:3


    for rr=1:length( room_array{pose})
        
             room=  room_array{pose}(rr) 
      
           
             label_name=sprintf('./Dataset/room%d/%s/class_label_all.mat',room,skel_file{pose}) ;% load file   
             load( label_name,'class_label_all');
            
         
             lab = class_label_all';
                                
                    
         
             feature_name=sprintf('./Dataset/room%d/%s/features_all.mat',room,skel_file{pose}) ;% load file 
         
             load( feature_name,'features');
             
             
             pos_features =( features(:,lab==1));
             
             if size( pos_features,2)>100
             pos_features=pos_features(:,1:100);
             end
             
             negative_features1= features(:,lab==-1);
             class_diff= size(negative_features1,2 )/size( pos_features,2);
            
            
            
             
             for class_multi=0:10
            
               class_multi
               neg_features=(  negative_features1(:,randperm(size(negative_features1,2 ),size( pos_features,2)*2^(class_multi))));
              
             
               features_svm=double([pos_features,( neg_features)]);
             
               label_svm=[ones(1,size(pos_features,2)),ones(1,size( neg_features,2))*(-1)];
             
               para= sprintf('-c  0.000001 -j %f ',size(neg_features,2)/size(pos_features,2));
           
        
          
               svm_model=  svmlearn(features_svm', label_svm', para);    
    
               model_name=sprintf('./Dataset/room%d/%s/model_svm_%d.mat',room,skel_file{pose},class_multi) ;% load file   
               save(model_name,'svm_model');
                   
                     
             end
    
      
    end 
end
   % system('pkill -u jmpiyath');
    
    
 
    
   
        
 
 
   


  
   
  
  
  
  
  