- Learnroom_main_svm is the main learning function
- Inferroom_main i sthe main inference function


 These files need  mat files generated from  the follwing script files


- create_skeleteons : creates skeleton pcd file for each pose
- create2_Yground : calculates the Torso position of the groundtruth  ss1.ply. A skeleton file need to be manually placed and should be created
  for each data set.

- creat3_fullmat_rotation_translation : creates rotation and translation for 1000 features.

- Create4_SitN Leg : creates mat files for sitting and leg files to create  grounddtruth
- create5_disAllRooms : creates distance maps and occupancy maps for each room. Modify map_dis according to the requirement.
