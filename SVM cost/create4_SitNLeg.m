


skel_file =cellstr(['relaxing';'leaning ';'working ']);




gTruth_Joint{2}=cellstr(['leg ';'hand']);
gTruth_Joint{1}=cellstr(['leg';'sit']);
gTruth_Joint{3}=cellstr(['leg';'sit']);
joint_array{1}=[12 15 10 13];
joint_array{3}=[4 7 10 13];
joint_array{2}=[12 15  6 9 ];

poses=2;

room_array{1}=[1,2];
room_array{2}=[8];
room_array{3}=[3,4,5,6,7];



for rr=1:size(room_array{poses},2)
  %for rr=1:1  
    room=room_array{poses}(rr)




% f_name=sprintf('./Dataset/living%d',ff(k));




 
joint1_mat_name =sprintf('./Dataset/room%d/%s/%s.mat',room,skel_file{poses},gTruth_Joint{poses}{1});
 
plyname=sprintf('./Dataset/room%d/%s/%s.ply',room,skel_file{poses},gTruth_Joint{poses}{1});
     
ply=plyread(plyname);

joint1mat =[ply.vertex.x,ply.vertex.y];

 joint2_mat_name =sprintf('./Dataset/room%d/%s/%s.mat',room,skel_file{poses},gTruth_Joint{poses}{2});
 
plyname=sprintf('./Dataset/room%d/%s/%s.ply',room,skel_file{poses},gTruth_Joint{poses}{2});
     
ply=plyread(plyname);

joint2mat =[ply.vertex.x,ply.vertex.y];





joint1=[];
joint2=[];


for n=0:length(joint1mat)/4-1
n
joint1{n+1}=joint1mat(4*n+1:4*n+4,:)

end

for n=0:length(joint2mat)/4-1
n
joint2{n+1}=joint2mat(4*n+1:4*n+4,:)

end



save( joint1_mat_name,'joint1');
save( joint2_mat_name,'joint2');


mapfile = sprintf('./Dataset/room%d/map.mat',room);  
        
            load(mapfile,'map');   
                  
            DM=map.dis;
            ocmap=map.oc;
            center=map.center;
            
            YG_name=sprintf('./Dataset/room%d/%s/YGround.mat',room,skel_file{poses});
            load(YG_name,'ymax');
            meanAll =(ymax);
            
            
            Zheight =meanAll(3)/10-center(3)+50-1;





skel_mat=sprintf('./full_mat/skel_points_%s.mat',skel_file{poses});   

load(skel_mat,'full_mat');

skel_mat=sprintf('./Dataset/room%d/%s/skel_points.mat',room,skel_file{poses});   

    


%Vskel2=load('Vskel.mat');
Vskel2=load(skel_mat);


Vskel1 =Vskel2.Vskel1;

P_length= length(Vskel1);







full_mat =full_mat+repmat([0, 0, Zheight,0 ],length(full_mat),1);
Torso_mat =full_mat(3:P_length:length(full_mat),:);
joint1A =full_mat(joint_array{poses}(1):P_length:length(full_mat),:);
joint1B = full_mat(joint_array{poses}(2):P_length:length(full_mat),:);
joint2A =full_mat(joint_array{poses}(3):P_length:length(full_mat),:);
joint2B =full_mat(joint_array{poses}(4):P_length:length(full_mat),:);


%class_lab=zeros(length(Torso_mat),1);
joint1_inA=zeros(length(Torso_mat),1);
joint1_inB=zeros(length(Torso_mat),1);
joint2_inA=zeros(length(Torso_mat),1);
joint2_inB=zeros(length(Torso_mat),1);

          for ss=1:size(joint1,2)
          joint1_t = joint1{ss}/10 +repmat([50-center(1) 50-center(2)],size(joint1{ss},1),1);
          
         
         joint1_inA= or (joint1_inA,inpolygon(joint1A(:,1),joint1A(:,2),joint1_t(:,1),joint1_t(:,2)));
         joint1_inB= or(joint1_inB,inpolygon(joint1B(:,1),joint1B(:,2),joint1_t(:,1),joint1_t(:,2)));
         
         
          end
          
          
         for ss1=1:size(joint2,2)
          
            
         joint2_t = joint2{ss1}/10+repmat([50-center(1) 50-center(2)],size(joint2{ss1},1),1);

         
         joint2_inA = or(joint2_inA, inpolygon(joint2A(:,1),joint2A(:,2),joint2_t(:,1),joint2_t(:,2)));
         joint2_inB = or(joint2_inB, inpolygon(joint2B(:,1),joint2B(:,2),joint2_t(:,1),joint2_t(:,2)));

        
          
         end
         
         
         All_sitting = (joint1_inA+joint1_inB+joint2_inA+joint2_inB);
         Issit =(All_sitting==4);

         class_lab =(Issit);
          
         
          
         
        % lab = class_lab*2-ones(length(class_lab),1);


         groundlabel_mat=sprintf('./Dataset/room%d/%s/class_lab.mat',room,skel_file{poses});   
         save(groundlabel_mat,'class_lab');
         
         lab = class_lab;
                         
         Sit= Torso_mat(lab',:);
        Sit_tra= (Sit(:,1:3)+repmat([-50+center(1) -50+center(2) -50+center(3)],size(Sit,1),1))*10;
        Sit_tra= [Sit_tra,Sit(:,4)]    

        
        % save(LLfile,'LL'); 
yymax=Sit_tra;

skelfile= sprintf('./Dataset/room%d/%s/skelGround.pcd',room,skel_file{poses});
Vskel_file=sprintf('./Dataset/room%d/%s/Vskel.mat',room,skel_file{poses}); 


Vskel2=load(Vskel_file);


Vskel3 =Vskel2.Vskel1;
global Vskel;
Vskel=Vskel3;
global mapresolution;
global skelresolution;
mapresolution=1;
skelresolution=1;
%x=[ 564.7350  187.4020  231.0440    5.1000] % ground truth

%xnew =[483.5000  219.6990  231.0440    4.1000];
%drawskeleton(xnew);
length(yymax)
if length(yymax)<20
drawskeletonAll(yymax,skelfile);


else
drawskeletonAll(yymax(1:round(length(yymax)/30):length(yymax),:),skelfile);
    
end
        
         
         
         
         




end
