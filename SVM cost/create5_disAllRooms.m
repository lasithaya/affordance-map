ff=[1,2,3,4,5,6,7,8];

global mapresolution;
mapresolution=1;



for k=1:8

 
 
plyname=sprintf('./Dataset/room%d/room%dN.ply',ff(k),ff(k));
     
ply=plyread(plyname);

room =[ply.vertex.x,ply.vertex.y,ply.vertex.z];

minroom =  min(room(:,1:3));
maxroom =  max(room(:,1:3));



A= room(:,1:3)/10 ;% using 10 cm grids, map is in cm units

normals=ply.vertex.nz;

%global center;
center =(max(A)+min(A))/2; % center of the 3d point cloud
A=A-repmat( center(1:3),length(A),1);

% transform the point cloud to the center



% room_cloud= [ply.vertex.x,ply.vertex.y,ply.vertex.z];
% 
% normals =[ply.vertex.nx,ply.vertex.ny,ply.vertex.nz];
% 
% [mm,I] =max(abs(normals'));
% 
% 
% %index =find(I==3);
% 
% 
% index=find(normals(:,3)>0.9);


%global ocmap; % occupancy grid

%global DM; % Distance map


% create the distance field for the full room


%[DM,ocmap]=map_dis(A(index,:)*mapresolution);

[DM,map_grid,ocmap]=map_dis(A*mapresolution,normals);

% YG_name=sprintf('./Dataset/room%d/%s/YGround.mat',ff(k),char(affor_name(affor_num)));  % load groundtruth
% load(YG_name,'ymax'); % load ground truth
%      
% meanAll =(ymax);








%map.features=sprintf('./Dataset/room%d/%s/feature.mat',ff(k),char(affor_name(affor_num))); 
%map.features_oc=sprintf('./Dataset/room%d/%s/feature_oc.mat',ff(k),char(affor_name(affor_num))); 

%map.Zheight=meanAll(3);
map.center =center;
map.dis=DM;
map.oc=ocmap;
map.minroom=minroom;
map.maxroom=maxroom;
map.grid=map_grid;
mapfile = sprintf('./Dataset/room%d/map.mat',ff(k));
save (mapfile,'map');
end