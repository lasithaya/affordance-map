 ff=[1,2,3,4,5,6];
affor_name =['relaxing'];
affor_name =cellstr(affor_name);
affor_num = 1;


for n=1:2
            
     
            mapfile = sprintf('./Dataset/living%d/%s/map.mat',ff(n),char(affor_name(affor_num)));  
        
            load(mapfile,'map');   

          
            
            DM=map.dis;
            center=map.center;
            Zheight =map.Zheight/10-center(3)+50-1;
            

            load('full_mat.mat','full_mat');

            full_mat =full_mat+repmat([0, 0, Zheight,0 ],length(full_mat),1);
            Torso_mat =full_mat(3:15:length(full_mat),:);
            legA =full_mat(14:15:length(full_mat),:);
            legB = full_mat(15:15:length(full_mat),:);
            HipA =full_mat(8:15:length(full_mat),:);
            HipB =full_mat(10:15:length(full_mat),:);
           % dis  =DM(full_mat(:,1:3));

           
            sit_file = sprintf('./Dataset/living%d/%s/sitting.mat',ff(n),char(affor_name(affor_num))); 
            leg_file = sprintf('./Dataset/living%d/%s/leg.mat',ff(n),char(affor_name(affor_num))); 
            load(sit_file,'sitting');
            load(leg_file,'leg');

% transform ground truth  leg polygon and hip polygon to dis map



% coordinates
          class_lab=zeros(length(Torso_mat),1);
          for ss=1:size(sitting,2)
          leg_t = leg{ss}/10 +repmat([50-center(1) 50-center(2)],size(leg{ss},1),1);
          sitting_t = sitting{ss}/10+repmat([50-center(1) 50-center(2)],size(sitting{ss},1),1);

          hips_inA = inpolygon(HipA(:,1),HipA(:,2),sitting_t(:,1),sitting_t(:,2));
          hips_inB = inpolygon(HipB(:,1),HipB(:,2),sitting_t(:,1),sitting_t(:,2));

         leg_inA= inpolygon(legA(:,1),legA(:,2),leg_t(:,1),leg_t(:,2));
         leg_inB= inpolygon(legB(:,1),legB(:,2),leg_t(:,1),leg_t(:,2));


         All_sitting = (hips_inA+hips_inB+leg_inA+leg_inB);
         Issit =(All_sitting==4);

         class_lab =or (class_lab,and((Torso_mat(:,3)==Zheight+1),Issit));
         
          end
         
         lab = class_lab;
                         
         Sit= Torso_mat(lab',:);
        Sit_tra= (Sit(:,1:3)+repmat([-50+center(1) -50+center(2) -50+center(3)],size(Sit,1),1))*10;
        Sit_tra= [Sit_tra,Sit(:,4)]    

        
        % save(LLfile,'LL'); 
yymax=Sit_tra;

skelfile= sprintf('./Dataset/living%d/%s/skelGround.pcd',ff(n),char(affor_name(affor_num)));

Vskel2=load('Vskel.mat');

Vskel3 =Vskel2.Vskel1;
global Vskel;
Vskel=Vskel3;
global mapresolution;
global skelresolution;
mapresolution=1;
skelresolution=1;
%x=[ 564.7350  187.4020  231.0440    5.1000] % ground truth

%xnew =[483.5000  219.6990  231.0440    4.1000];
%drawskeleton(xnew);
length(yymax)
if length(yymax)<20
drawskeletonAll(yymax,skelfile);


else
drawskeletonAll(yymax(1:round(length(yymax)/30):length(yymax),:),skelfile);
    
end
        
        
        
        
        
        
        
        
        
        
         
          end