function [yymax,lossmin]=inferRoom()



Vskel2=load('Vskel.mat');

Vskel1 =Vskel2.Vskel1;
global mapresolution;
mapresolution=1;

 
 ff=[1,3,4,5,6,7];

 for k=1:1
    k
     
    model_name=sprintf('./Dataset/living%d/model.mat',ff(k));
    
    load(model_name,'model');   
     
    mapfile = sprintf('./Dataset/living%d/map.mat',ff(k));  
        
    load(mapfile,'map');   
 
 
    YG_name=sprintf('./Dataset/living%d/YGround.mat',ff(k));  % load groundtruth
    
    load(YG_name,'ymax'); % load ground truth
     
    y = ymax;
    
    
    
 
 
      
      tic;
  DM=map.dis;  
  center =map.center;
  minroom =  map.minroom;
  maxroom =  map.maxroom;
  xx= minroom(1):10:maxroom(1);
  yy= minroom(2):10: maxroom(2) ;
  zz= -10:5:10;
  
  theta=0:0.1:2*pi;
  cc=zeros(length(xx),length(yy),length(zz),length(theta));
  LL=zeros(length(xx),length(yy),length(zz),length(theta));
  mapr=mapresolution;
  Zheight=map.Zheight;
  
  

      W=model.w;
     
      
% slack resaling: argmax_y delta(yi, y) (1 + <psi(x,y), w> - <psi(x,yi), w>)
% margin rescaling: argmax_y delta(yi, y) + <psi(x,y), w>
 

 
 parfor i=1:length(xx)
     
  
  
 [cc(i,:,:,:),LL(i,:,:,:)]=inloop(xx,yy,zz,theta,y,i,W,Vskel1,DM,center,mapr,Zheight);
     
    
 
 end
 
 
% [cc1,tt]= max(cc,[],3);
 
 
 
 
%  [v,ind2]=max(cc1(:));
%   ind =find(cc1(:)>(v-100));
%  
%   [x11,y11] = ind2sub(size(cc1),ind);
%   
%   theta11=tt(sub2ind(size(tt),x11,y11));
%   
%   zz= 231.0440*ones(length(x11),1);

[v,ind2]=max(cc(:))

ind =find(cc(:)>=(v-70));

[x11,y11,zz11,theta11] = ind2sub(size(cc),ind);
zzz= Zheight*ones(length(x11),1)+zz(zz11)';
  
yymax =[xx(x11)',yy(y11)',zzz,theta(theta11)'];

lossmin=LL(ind);

ccfile = sprintf('./Dataset/living%d/cc.mat',ff(k));  
save(ccfile,'cc');

LLfile = sprintf('./Dataset/living%d/LL.mat',ff(k));  
save(LLfile,'LL'); 

skelfile= sprintf('./Dataset/living%d/skel%d.pcd',ff(k),ff(k));

Vskel2=load('Vskel.mat');

Vskel3 =Vskel2.Vskel1;
global Vskel;
Vskel=Vskel3;
global mapresolution;
global skelresolution;
mapresolution=1;
skelresolution=1;
%x=[ 564.7350  187.4020  231.0440    5.1000] % ground truth

%xnew =[483.5000  219.6990  231.0440    4.1000];
%drawskeleton(xnew);
length(yymax)
if length(yymax)<20
drawskeletonAll(yymax,skelfile);


else
drawskeletonAll(yymax(1:round(length(yymax)/10):length(yymax),:),skelfile);
    
end

 end
end


  
  function [cc1,LL1]=inloop(xx,yy,zz,theta,y,i,W,Vskel1,DM,center,mapresolution,Zheight)
    
  
    cc1=zeros(length(yy),length(zz),length(theta));
    LL1=zeros(length(yy),length(zz),length(theta));
    xxs=xx(i);
     Alltheta =zeros(1,length(theta));
     AllLoss=zeros(1,length(theta));
     for j=1:length(yy) 
          
     
          for z=1:length(zz)
          
                for tt=1:length(theta)
             
                
              
                    ybar=[xxs, yy(j) ,Zheight+zz(z) ,theta(tt) ];
                     
                    Dfeat=getSkelFeatures(ybar,Vskel1,DM,center,mapresolution);
                    Dfeat=sparse(double(Dfeat));

                    %cc(k)= dot(getSkelFeatures(ybar), model.w)+ double(sum((getSkelFeatures(y)-getSkelFeatures(ybar)).^2));
                    Alltheta(tt)=dot(Dfeat,W);
                    AllLoss(tt)=1;
                end
               cc1(j,z,:)=Alltheta;
               LL1(j,z,:)=AllLoss;
          end
        
          
         
     end
  end
  
  
  function delta = lossCB(y, ybar)
  %delta = double(sum((getSkelFeatures(y,Vskel1,DM,center,mapresolution)-getSkelFeatures(ybar,Vskel1,DM,center,mapresolution)).^2)) ;
  
   % y is the groundtruth
   Vskel2=load('Vskel.mat');
   Vskel1 =Vskel2.Vskel1;
      hip_poly =[8,9,11,10];


    rotmat=rotation(ybar(4));     
     
    % new tranformed position

    skely =(rotmat*Vskel1)'+repmat([ybar(1) ybar(2) ybar(3) ],length(Vskel1'),1);
    
    P1.x=skely(hip_poly, 1)'; P1.y=skely(hip_poly, 2)'; P1.hole=0;
 
    
    
    loss_arr =ones(size(y,1),1)*10;
    
 for n=1: size(y,1)
    
    
    
    
    rotmat=rotation(y(n,4));

    % new tranformed position

    skelybar =(rotmat*Vskel1)'+repmat([y(n,1) y(n,2) y(n,3) ],length(Vskel1'),1);
    
    P2.x=skelybar(hip_poly, 1)'; P2.y=skelybar(hip_poly, 2)'; P2.hole=0;
    
    
    
    P3=PolygonClip(P1,P2,1);
    % calculate intersecting area..
    % calculate Union area
    
    
    % find Z direction intersection
    
    
       
       zabs =abs(ybar(3)-y(n,3));
    
       
    
    
    


        if (isempty(P3) || zabs>20);
        Loss=1;
    
        else
%             P4=PolygonClip(P1,P2,3);
%             plot(P3.x,P3.y,'r');
%             hold on
%             plot(P1.x,P1.y,'b');
%             plot(P2.x,P2.y,'y');
%             plot(P3.x,P3.y,'r');

        Inter = polyarea(P3.x,P3.y);
       % Union= polyarea(P4.x,P4.y);
        Loss =(1-Inter/1265);     
    
    
    
        end
      
        loss_arr(n,1)= Loss;
        
        
 end   
        
     [LLmin,I]=min(loss_arr);
     
     if LLmin <0.7
         
         delta=I;
         
     else
         delta=0;  
     end
    % delta=LLmin;
         
         
          
  
  end

  
  
  
  
