clear


skel_file =cellstr(['relaxing';'leaning ';'working ']);

affordance_type=1;

%affordance{1} =[4,6,5]; %sitting

gTruth_Joint{2}=cellstr(['leg ';'hand']);
gTruth_Joint{1}=cellstr(['leg';'sit']);
gTruth_Joint{3}=cellstr(['leg';'sit']);

room_array{1}=[1,2,11];
room_array{2}=[5,6,8,9,10];
room_array{3}=[3,4,5,6,7];





%format long;
for pose=1:3


 Fscore=[];

 for rr=1:length( room_array{pose})
        
   room=  room_array{pose}(rr) 
    
        
 for model_num=3:3
    
        
           room
         
           skel_mat=sprintf('./full_mat/skel_points_%s.mat',skel_file{(pose)});   

           load(skel_mat,'full_mat');

           Torso_mat =full_mat(3:15:length(full_mat),:);
                    
           full_mat=[];          
                       

           feature_name=sprintf('./Dataset/room%d/%s/features_1000.mat',room,skel_file{(pose)});  % load file
         
         
         
           features=load(feature_name,'features');
           features=(features.features);
         
           
           groundlabel_mat=sprintf('./Dataset/room%d/%s/class_lab.mat',room,skel_file{(pose)});   
           load(groundlabel_mat,'class_lab');
                
                
           model_name_pos=sprintf('./Dataset/room%d/%s/model_svm_%d.mat',room,skel_file{(pose)},model_num) ;% load file
           load( model_name_pos,'svm_model');
         
           class_label= (class_lab>0)*1+ (class_lab==0)*-1;
                
                
                
           P_length= size(features,1);
           
           multi=101*101*3;
           ang_multi=63;
         
          
         
           TTT=mat2cell(features,P_length,101*101*1*ones(1,63));
           class_TT=mat2cell(class_label,101*101*1*ones(1,63),1);
           tic
           
            parfor ii=1: ang_multi
           
              [error,pred]=svmclassify(double(TTT{1,ii})', class_TT{ii},svm_model);
              prediction{ii}=pred'
               
         
            
            end
                
           toc          
                
          
            
          predictions=cell2mat( prediction);      
                
          predictions_pos=  predictions.* ( predictions>0);  
            
       
           
          Pred_pos = ( predictions>0)';
          Pred_neg = ~Pred_pos;
           
          
          pred_mesh=reshape( predictions_pos,[63,101*101]);
         
          [pred_mesh,pred_index] =max(pred_mesh);
         
          pred_mesh=reshape((pred_mesh),[101,101]);
          pred_index=reshape((pred_index),[101,101]);
         
       
         ground_truth=reshape(class_lab,[63,101*101]);
        
         [ground_truth, ground_index]= max(ground_truth);
         ground_truth=reshape((ground_truth),[101,101]);
         
         ground_index=reshape((ground_index),[101,101]);
          
         
          
           
         FP=  and(~ground_truth , pred_mesh);
         FN=  and(ground_truth , ~ pred_mesh); 
         
         TP = and(ground_truth , pred_mesh );
           
      
         FP_sum(iter+1) = sum(FP(:))
         FN_sum(iter+1) = sum(FN(:))
         TP_sum(iter+1) = sum(TP(:))
         
         
         Fscore(iter+1) = 2*TP_sum((iter+1)) /(2*TP_sum((iter+1)) +FP_sum((iter+1))+FN_sum((iter+1)))
    
          
         mapfile = sprintf('./Dataset/room%d/map.mat',room);  
      
         load(mapfile,'map');   
         min_room =map.minroom/10;
         max_room=map.maxroom/10;
         center=map.center;
         
         max_plot= max_room-center+50;
         min_plot= min_room-center+50;
         
         joint2_mat_name =sprintf('./Dataset/room%d/%s/%s.mat',room,skel_file{(pose)},gTruth_Joint{pose}{2});

         load(joint2_mat_name,'joint2'); 
         
         
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Figure 1     
         
       
         C=drawroom(room);
         figure
        % subplot(1,3,1)
         axis equal tight
         axis([ min_plot(1)  max_plot(1) min_plot(1)  max_plot(1)])
         image(C);
         axis equal tight
         axis([ min_plot(1)*10-10  max_plot(1)*10+10 min_plot(1)*10-10  max_plot(1)*10+10])
         hold on
         
         for t=1:size(joint2,2)
             
           sit= joint2{t} ;   
         
           sit=sit+repmat([50-center(1) 50-center(2)],size(sit,1),1)*10;
           sitH=fill(sit(:,2)',sit(:,1)','g')  
           hold on   
         end
         
         
         
        ground_ind = find(pred_mesh>0);
         
        s = [101,101];
        [ground_x,ground_y] = ind2sub(s,ground_ind); 
        pred_loc= Torso_mat(Pred_pos,:);
    
        ground_angle= pred_loc(:,4);
       
         
        gH= scatter(ground_x*10,ground_y*10,50,'r','filled');
         
        angle_array= [(0:0.1:2*pi),(0:0.1:2*pi),(0:0.1:2*pi)];
        
        
        for a=1:size(ground_x,1)
            
        angle=angle_array(pred_index(ground_x(a),ground_y(a)));
    
        plot([ground_x(a)*10 ground_x(a)*10+10*sin(angle) ],[ground_y(a)*10 ground_y(a)*10+10*cos(angle)],'b','LineWidth',2);
        
        end
         
        len=legend([gH,sitH],'Torso Position','Table Tops');
        set(len,'FontSize',12,'Location','southeast');
        title('Ground Truth','FontSize',12);
         
        axis([ min_plot(1)*10-10  max_plot(1)*10+10 min_plot(1)*10-10  max_plot(1)*10+10])
        title('Predictions','FontSize',12);
        set(gcf, 'PaperUnits', 'inches');
        set(gcf, 'PaperSize', [6.00 6.00]);
        set(gcf, 'PaperPositionMode', 'manual');
        set(gcf, 'PaperPosition', [0 0 6.00 6.00]);
        
        
         
        file_name= sprintf('./Dataset/room%d/%s/svm_%s_prediction_%d.pdf',room,skel_file{(pose)},skel_file{(pose)},iter);
        print(gcf, '-dpdf', file_name); 
         
         
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  Figure 2
%          
%          
%          
         figure
         %subplot(1,3,2)
         axis equal tight
         hold on
         ground_pred=reshape(predictions,[63,101*101]);
         ground_pred=reshape(max(ground_pred),[101,101]);
           colorbar
        % ground_pred=flipud(ground_pred');
         contourf((1:101)*10,(1:101)*10,ground_pred')
         colorbar
         set(gca,'ydir','reverse')
         axis([ min_plot(1)*10-10  max_plot(1)*10+10 1000-max_plot(1)*10-10 1000- min_plot(1)*10+10])
         
        for t=1:size(joint2,2)
          sit= joint2{t} ;   
         
          sit=sit+repmat([50-center(1) 50-center(2)],size(sit,1),1)*10;
          plot([sit(:,2);sit(1,2)]',[sit(:,1);sit(1,1)]','w','LineWidth',2)  
         
          hold on   
        end 
        
        title('Decision Function','FontSize',12);
        set(gcf, 'PaperUnits', 'inches');
        set(gcf, 'PaperSize', [6.00 6.00]);
        set(gcf, 'PaperPositionMode', 'manual');
        set(gcf, 'PaperPosition', [0 0 6.00 6.00]);
        
        
         
        file_name= sprintf('./Dataset/room%d/%s/svm_%s_decision.pdf',room,skel_file{(pose)},skel_file{(pose)});
        print(gcf, '-dpdf', file_name); 
        
  
 
   file_name= sprintf('./Dataset/room%d/%s/svm_imbalance.mat',room,skel_file{(pose)});
   imbalance.Fscore=Fscore
   imbalance.FP=FP_sum
   imbalance.FN=FN_sum
   imbalance.TP=TP_sum
   save(file_name,'imbalance');
  
  end
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
%  draw skeletons
 
           


    mapfile = sprintf('./Dataset/room%d/map.mat',room);  
        
    load(mapfile,'map');   
                  
    center=map.center;
          
          
     YG_name=sprintf('./Dataset/room%d/%s/YGround.mat',room,skel_file{(pose)});
     load(YG_name,'ymax');     
          

     Zheight =ymax(3)/10-center(3)+50-1;

       
     Torso_mat =Torso_mat+repmat([0, 0, Zheight,0 ],length(Torso_mat),1);   
        
         
     I= find(predictions>0)  ;  
      
 
   LLL=length(I)
 
  %keyboard
  Sit= Torso_mat(I',:);
  Sit_tra= (Sit(:,1:3)+repmat([-50+center(1) -50+center(2) -50+center(3)],size(Sit,1),1))*10;
  Sit_tra= [Sit_tra,Sit(:,4)] ;   
           
  yymax=Sit_tra;

 

 Vskel_file=sprintf('./Dataset/room%d/%s/Vskel.mat',room,skel_file{(pose)}); 


 Vskel2=load(Vskel_file);


 Vskel3 =Vskel2.Vskel1;
 
 
 
 global Vskel;
 Vskel=Vskel3;
 global mapresolution;
 global skelresolution;
 mapresolution=1;
 skelresolution=1;



 length(yymax)

 skelfile=sprintf('./Dataset/room%d/%s/infer.pcd',room,skel_file{(pose)});   

  if length(yymax)<20
     drawskeletonAll(yymax,skelfile);


  else
    drawskeletonAll(yymax(1:round(length(yymax)/20):length(yymax),:),skelfile);
    
  end   
  

 end
end 
  
 
    
    
   
   keyboard


            
      
              
         II= prob_all';
         
         a= sum(class_lab .* II);
         c=  sum(~class_lab .* II);
         b= sum(class_lab .* ~II);
         d= sum(~class_lab .*~II);
         
         Fscore= 2*a/(2*a+b+c)
         
         
         performance =[a,b,c,d, a/(a+b),b/(a+b) Fscore];
         
         perfor_file=sprintf('./Dataset/room%d/%s/perfor.mat',room,skel_file{affordance{affordance_type}(pose)});   
         
         
         save( perfor_file,'performance'); 
              
         
                
        
     
        
        
          
             
    

        
        
         
         
        
  
  
  
  
  
    

  
  
 
  
 
  

      
      
      
      







  
  
  
  
