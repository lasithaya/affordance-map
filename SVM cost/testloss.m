Vskel2=load('Vskel.mat');

load('sitting.mat','sitting');
load('leg.mat','leg');
%ybar =[644.4480  -98.6902  271.1580    1.4300];
ybar =[726.1930  -98.6902  271.1580    1.4300];

rotmat=rotation(ybar(4));     

Vskel1 =Vskel2.Vskel1;
  
skelybar =(rotmat*Vskel1)'+repmat([ybar(1) ybar(2) ybar(3) ],length(Vskel1'),1);
  
skel_legs = skelybar(14:15,:);
skel_hips =skelybar([8,10],:);

hips_in = inpolygon(skel_hips(:,1),skel_hips(:,2),sitting(:,1),sitting(:,2));
leg_in =  inpolygon(skel_legs(:,1),skel_legs(:,2),leg(:,1),leg(:,2));

Interarea=0;

if sum(hips_in)>0 && sum(leg_in)>0
    hip_poly =[8,9,11,10];
    
   P2.x=skelybar(hip_poly, 1)'; P2.y=skelybar(hip_poly, 2)'; P2.hole=0;
   P1.x=sitting(:, 1)'; P1.y=sitting(:, 2)'; P1.hole=0;
   P3=PolygonClip(P1,P2,1);
   Interarea = polyarea(P3.x,P3.y)/1217;
     
end

     if (Interarea==0 && ybar(5)==1 )     
     delta=1;  
     end 
        
     if (Interarea==0 && ybar(5)==-1 )       
     delta=0;  
     end 
        
     if (Interarea>0 && ybar(5)==1)        
     delta=1-Interarea;
       
     end 
        
     if (Interarea>0 && ybar(5)==-1)        
     delta=1;  
     end

  
  