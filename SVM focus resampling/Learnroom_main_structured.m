function [model]= Learnroom_main_structured
skel_file =cellstr(['relaxing';'leaning ';'working ']);
global pose;
pose=3;
format long;

global room;

    for room=7:7
        
         
    
    
          
          
             
              
         
            % groundlabel_mat=sprintf('./Dataset/room%d/%s/class_lab.mat',ff(n),skel_file{affordance{affordance_type}(pose)});   
            % load(groundlabel_mat,'class_lab');
            
              
    
    
             label_name=sprintf('./Dataset/room%d/%s/class_label_all.mat',room,skel_file{pose}) ;% load file   
             load( label_name,'class_label_all');
            
         
             lab = class_label_all';
                                
          
             labels =num2cell(lab');       
                    
         
             feature_name=sprintf('./Dataset/room%d/%s/features_all.mat',room,skel_file{pose}) ;% load file 
         
            features=load(feature_name,'features');
            features=features.features;
             
             
             
             patterns=num2cell( features,1);
    
            
           
    
     parm.patterns = patterns ;
     parm.labels = labels ;
     parm.lossFn = @lossCB ;
     parm.constraintFn  = @constraintCB ;
     parm.featureFn = @featureCB ;
%   parm.endIterationFn = @iterCB ;
     parm.dimension = 2000 ;
     parm.verbose = 1 ;
     model = svm_struct_learn(' -c 100  -v 1 -e 1', parm) ;
 
          
    model_name=sprintf('./Dataset/room%d/%s/model_struct.mat',room,skel_file{pose}) ;% load file   
    save(model_name,'model');
    
     
 
    end 
   
   % system('pkill -u jmpiyath');
    
    
end  
    
 function psi = featureCB(param, x, y)
     
     
 %   fprintf('feature function \n')  
  
  

     psi =sparse( y*double(x)/2 );
    
    

  %  fprintf('********************************************\n')  
    

 end        
        
 
 function delta = lossCB(param, y, ybar)
  
  global room
 delta = double(y ~= ybar) ;

   room
 
  end
        
 function yhat = constraintCB(param, model, x, y)
   
   
   
  fprintf('constrain function\n')    
      
  if dot(y*double(x), model.w) > 1, yhat = y ; else yhat = - y ; end
    
 


%fprintf('********************************************\n')     
        
 end
        
        
        



  
 
 


  
  




  
 
  
  
  
  
  
  
  
  
  
  
  
  
  