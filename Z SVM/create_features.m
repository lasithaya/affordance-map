
clear
skel_file =cellstr(['reaching';'sitting ';'standing';'relaxing';'leaning ';'working ']);
poses=4;

for room=1:6
    ff=[1,2,3,4,5,6];
    feature_name=sprintf('./Dataset/room%d/features_1000.mat',ff(file));  % load file
    
    
     
            mapfile = sprintf('./Dataset/room%d/map.mat',ff(file));  
        
            load(mapfile,'map');   
                  
            DM=map.dis;
            center=map.center;
           
            
             YG_name=sprintf('./Dataset/room%d/%s/YGround.mat',room,skel_file{poses});
            load(YG_name,'ymax');
            meanAll =(ymax);
            
            
            Zheight =meanAll(3)/10-center(3)+50-1;
            
            
            
            
            
            
            
            features=int8(zeros(1000, 101*101*63*3));
            
            
 for tt=0:9
      tt
mapfile = sprintf('full_mat_%d.mat',tt);  
  
  
  
  
 full_mat=load(mapfile,'full_mat');
 
  full_mat=full_mat.full_mat;

  full_mat =single(full_mat)/100+repmat([0, 0, Zheight ],length(full_mat),1);
  
  
  tic
  dis =double(DM(full_mat));
  
%   dis1  =double(DM(full_mat(1:length(full_mat)/2,1:3)));
%   dis2  =double(DM(full_mat(length(full_mat)/2+1:length(full_mat),1:3)));
%   toc
  clearvars 'full_mat';
 
%   dis=[dis1;dis2];
%  
%    clearvars 'dis1';
%    clearvars 'dis2';
  %Dis_shape =reshape(dis,100,length(dis)/100);
  features(tt*100+1:tt*100+100,:)=sign(reshape(dis,100,length(dis)/100));
  clearvars 'dis';
%    Dis_shape =dis_check;
  clearvars 'full_mat';

  end ; 
  
  
  save(feature_name,'features');
            
            
            
            
            
            
            
            
            
            
            
            
end