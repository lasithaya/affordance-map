
clear
skel_file =cellstr(['relaxing';'leaning ';'working ']);
poses=2;
room_array{1}=[1,2];
room_array{2}=[5,6,8];
room_array{3}=[3,4,5,6,7];
global mapresolution
mapresolution=1
for rr=1:size(room_array{poses},2)
    room=room_array{poses}(rr)
    feature_name=sprintf('./Dataset/room%d/%s/features_1000.mat',room,skel_file{poses});  % load file
    
    
     
            mapfile = sprintf('./Dataset/room%d/map.mat',room);  
        
            load(mapfile,'map');   
                  
            OC=map.oc;
            OC2=map.dis;
            center=map.center;
           
            
            YG_name=sprintf('./Dataset/room%d/%s/YGround.mat',room,skel_file{poses});
            load(YG_name,'ymax');
            meanAll =(ymax);
            
            
            Zheight =meanAll(3)/10-center(3)+50;
            
            
            
            
            
            
            
            features=int8(zeros(1000, 101*101*63*1));
            features2=int8(zeros(1000, 101*101*63*1));
            
            
 for tt=0:9
      tt
mapfile = sprintf('full_mat_%d.mat',tt);  
  
  
  
  
 full_mat=load(mapfile,'full_mat');
 
  full_mat=full_mat.full_mat;

  full_mat =round(single(full_mat)/100+repmat([0, 0, Zheight ],length(full_mat),1))*mapresolution;
  
  
  tic
  dis =int8((OC(full_mat)));
  dis2=int8((OC2(full_mat)));
  
%   dis1  =double(DM(full_mat(1:length(full_mat)/2,1:3)));
%   dis2  =double(DM(full_mat(length(full_mat)/2+1:length(full_mat),1:3)));
%   toc
  clearvars 'full_mat';
 
%   dis=[dis1;dis2];
%  
%    clearvars 'dis1';
%    clearvars 'dis2';
  %Dis_shape =reshape(dis,100,length(dis)/100);
  features(tt*100+1:tt*100+100,:)=(reshape(dis,100,length(dis)/100));
  features2(tt*100+1:tt*100+100,:)=(reshape(dis2,100,length(dis)/100));
  clearvars 'dis';
  clearvars 'dis2';
%    Dis_shape =dis_check;
  clearvars 'full_mat';

  end ; 
  
  features=[features;features2];
  
  
  save(feature_name,'features');
            
            
            
            
            
            
            
            
            
            
            
            
end