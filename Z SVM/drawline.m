function [B]= drawline( a,b,r )

x = a(1) : (b(1)-a(1)+0.1)/1000 : b(1)+0.1;
y = a(2) : (b(2)-a(2)+0.1)/1000 : b(2)+0.1;
z = a(3) : (b(3)-a(3)+0.1)/1000 : b(3)+0.1;
%drawCylinder([a(1) a(2) a(3) b(1) b(2) b(3) r],'open');
plot3(x, y, z);
xlabel('X');
ylabel('Y');
zlabel('Z');
%surf([x;y;z]); 
B=[x;y;z]';
end

