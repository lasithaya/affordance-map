clear


skel_file =cellstr(['relaxing';'leaning ';'working ']);

affordance_type=1;

%affordance{1} =[4,6,5]; %sitting

gTruth_Joint{2}=cellstr(['leg ';'hand']);
gTruth_Joint{1}=cellstr(['leg';'sit']);
gTruth_Joint{3}=cellstr(['leg';'sit']);

room_array{1}=[1,2,11];
room_array{2}=[5,6,8,9,10];
room_array{3}=[3,4,5,6,7];





for pose=1:3

    pose

 for rm=1:length(room_array{pose})
        
         room=room_array{pose}(rm)

%format long;


Fscore=[];

 
  for iter=0:10
    
    
        
           room
         
              skel_mat=sprintf('./full_mat/skel_points_%s.mat',skel_file{(pose)});   

           load(skel_mat,'full_mat');

            Torso_mat =full_mat(3:15:length(full_mat),:);
                    
           full_mat=[];          
          
%                 
%                 
%                 
%                 

          
        feature_name=sprintf('./Dataset/room%d/%s/features_1000.mat',room,skel_file{(pose)});  % load file
         
         
          
       
    
         
         features=load(feature_name,'features');
         features=(features.features);
         
         
% 
       
         
         
         %features2=(features(:,:));
                
        


                 
         
                groundlabel_mat=sprintf('./Dataset/room%d/%s/class_lab.mat',room,skel_file{(pose)});   
                load(groundlabel_mat,'class_lab');
                
                
                model_name_pos=sprintf('./Dataset/room%d/%s/model_svm_%d.mat',room,skel_file{(pose)},iter) ;% load file
                load( model_name_pos,'svm_model');
         
                class_label= (class_lab>0)*1+ (class_lab==0)*-1;
                
                
                
                P_length= size(features,1);
           
           
           
           
            multi=101*101*3;
            ang_multi=63;
         
           
         
         
         
         
           TTT=mat2cell(features,P_length,101*101*1*ones(1,63));
           class_TT=mat2cell(class_label,101*101*1*ones(1,63),1);
           clear prediction;
           tic
           
            parfor ii=1: ang_multi
           
              [error,pred]=svmclassify(double(TTT{1,ii})', class_TT{ii},svm_model);
              prediction{ii}=pred'
               
         %   ii
            
            end
                
            toc          
                
                
                
            
          predictions=cell2mat( prediction);      
                
            predictions_pos=  predictions.* ( predictions>0);  
            
       
           
          Pred_pos = ( predictions>0)';
          Pred_neg = ~Pred_pos;
           
          
         pred_mesh=reshape( predictions_pos,[63,101*101]);
         
         [pred_mesh,pred_index] =max(pred_mesh);
         
         pred_mesh=reshape((pred_mesh),[101,101]);
         pred_index=reshape((pred_index),[101,101]);
         
         
         
         
         
         
         ground_truth=reshape(class_lab,[63,101*101]);
        
         [ground_truth, ground_index]= max(ground_truth);
         ground_truth=reshape((ground_truth),[101,101]);
         
         ground_index=reshape((ground_index),[101,101]);
          
         
          
           
         FP=  and(~ground_truth , pred_mesh);
         FN=  and(ground_truth , ~ pred_mesh); 
         
         TP = and(ground_truth , pred_mesh );
           
      
         FP_sum(iter+1) = sum(FP(:))
         FN_sum(iter+1) = sum(FN(:))
         TP_sum(iter+1) = sum(TP(:))
         
         
         Fscore(iter+1) = 2*TP_sum((iter+1)) /(2*TP_sum((iter+1)) +FP_sum((iter+1))+FN_sum((iter+1)))
    
          
         mapfile = sprintf('./Dataset/room%d/map.mat',room);  
         
   file_name= sprintf('./Dataset/room%d/%s/svm_imbalance.mat',room,skel_file{(pose)});
  imbalance.Fscore=Fscore
  imbalance.FP=FP_sum
  imbalance.FN=FN_sum
  imbalance.TP=TP_sum
  save(file_name,'imbalance');
         
         
         
        
%           load(mapfile,'map');   
%           min_room =map.minroom/10;
%           max_room=map.maxroom/10;
%           center=map.center;
%          
%         max_plot= max_room-center+50;
%         min_plot= min_room-center+50;
% %          
%          joint2_mat_name =sprintf('./Dataset/room%d/%s/%s.mat',room,skel_file{(pose)},gTruth_Joint{pose}{2});
% 
%          load(joint2_mat_name,'joint2'); 
% %          
% %          
% %     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Figure 1     
%          
%        
%          C=drawroom(room);
%          
%         % subplot(1,3,1)
%          axis equal tight
%          axis([ min_plot(1)  max_plot(1) min_plot(1)  max_plot(1)])
%          image(C);
%          axis equal tight
%           axis([ min_plot(1)*10-10  max_plot(1)*10+10 min_plot(1)*10-10  max_plot(1)*10+10])
%          hold on
%          
%          for t=1:size(joint2,2)
%              
%          sit= joint2{t} ;   
%          
%          sit=sit+repmat([50-center(1) 50-center(2)],size(sit,1),1)*10;
%          sitH=fill(sit(:,2)',sit(:,1)','g')  
%           hold on   
%          end
%          
%          
%          
%          ground_ind = find(pred_mesh>0);
%          
%          s = [101,101];
%         [ground_x,ground_y] = ind2sub(s,ground_ind); 
%           pred_loc= Torso_mat(Pred_pos,:);
% %        ground_x= pred_loc(:,2);
% %        ground_y= pred_loc(:,1);
% %        ground_angle= pred_loc(:,4);
%        
%          
%         gH= scatter(ground_x*10,ground_y*10,50,'r','filled');
%          
%         angle_array= [(0:0.1:2*pi),(0:0.1:2*pi),(0:0.1:2*pi)];
%         
%         
%         for a=1:size(ground_x,1)
%             
%         angle=angle_array(pred_index(ground_x(a),ground_y(a)));
%       % angle=ground_angle(a);
%         plot([ground_x(a)*10 ground_x(a)*10+10*sin(angle) ],[ground_y(a)*10 ground_y(a)*10+10*cos(angle)],'b','LineWidth',2);
%         
%         end
%          
%          len=legend([gH,sitH],'Torso Position','Table top')
%         set(len,'FontSize',12,'Location','southeast');
%        % title('Ground Truth','FontSize',12);
%          
%         axis([ min_plot(1)*10-10  max_plot(1)*10+10 min_plot(1)*10-10  max_plot(1)*10+10])
%         title('Predictions','FontSize',12);
%         set(gcf, 'PaperUnits', 'inches');
%         set(gcf, 'PaperSize', [6.00 6.00]);
%         set(gcf, 'PaperPositionMode', 'manual');
%         set(gcf, 'PaperPosition', [0 0 6.00 6.00]);
%         
%         
%          
%         file_name= sprintf('./Dataset/room%d/%s/svm_%s_prediction_%d.pdf',room,skel_file{(pose)},skel_file{(pose)},iter);
%         print(gcf, '-dpdf', file_name); 
%          
         
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  Figure 2
         
         
         
%          figure
%          %subplot(1,3,2)
%           axis equal tight
%          hold on
%          ground_pred=reshape(predictions,[63,101*101]);
%          ground_pred=reshape(max(ground_pred),[101,101]);
%          
%         % ground_pred=flipud(ground_pred');
%          contourf((1:101)*10,(1:101)*10,ground_pred')
%          set(gca,'ydir','reverse')
%          axis([ min_plot(1)*10-10  max_plot(1)*10+10 1000-max_plot(1)*10-10 1000- min_plot(1)*10+10])
%         for t=1:size(joint2,2)
%          sit= joint2{t} ;   
%          
%          sit=sit+repmat([50-center(1) 50-center(2)],size(sit,1),1)*10;
%          plot([sit(:,2);sit(1,2)]',[sit(:,1);sit(1,1)]','w','LineWidth',2)  
% %         conH= fill(sit(:,2)',sit(:,1)','w') ;
% %         alpha(conH,0.5);
% %          
%          hold on   
%         end 
%          title('Decision Function','FontSize',12);
%         set(gcf, 'PaperUnits', 'inches');
%         set(gcf, 'PaperSize', [6.00 6.00]);
%         set(gcf, 'PaperPositionMode', 'manual');
%         set(gcf, 'PaperPosition', [0 0 6.00 6.00]);
%         
%         
%          
%         file_name= sprintf('./Dataset/room%d/%s/svm_%s_decision.pdf',room,skel_file{(pose)},skel_file{(pose)});
%         print(gcf, '-dpdf', file_name); 
%         
%    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  Figure 3
%         
%          
%         figure
%         
%          axis equal tight
%          image(C);
%          hold on
%          axis equal tight
%          ground_ind = find( ground_truth>0);
%          
%          s = [101,101];
%          [ground_x,ground_y] = ind2sub(s,ground_ind); 
%         
%          for t=1:size(joint2,2)
%              
%          sit= joint2{t} ;   
%          
%          sit=sit+repmat([50-center(1) 50-center(2)],size(sit,1),1)*10;
%          sitH= fill(sit(:,2)',sit(:,1)','g');  
%          hold on   
%          end
%          
%          
%         gH= scatter(ground_x*10-10,ground_y*10-10,50,'r','filled');
%         angle_array= [(0:0.1:2*pi),(0:0.1:2*pi),(0:0.1:2*pi)];
%         
%         
%         for a=1:size(ground_x,1)
%             
%        angle=angle_array(ground_index(ground_x(a),ground_y(a)));
%        T_Ori= plot([ground_x(a)*10-10 ground_x(a)*10-10+10*sin(angle) ],[ground_y(a)*10-10 ground_y(a)*10-10+10*cos(angle),],'LineWidth',2);
%         
%         end
%         
%         axis([ min_plot(1)*10-10  max_plot(1)*10+10 min_plot(1)*10-10  max_plot(1)*10+10])
%         len=legend([gH,sitH],'Torso Position','Table top')
%         set(len,'FontSize',12,'Location','southeast');
%         title('Ground Truth','FontSize',12);
%          
%         
%         set(gcf, 'PaperUnits', 'inches');
%         set(gcf, 'PaperSize', [6.00 6.00]);
%         set(gcf, 'PaperPositionMode', 'manual');
%         set(gcf, 'PaperPosition', [0 0 6.00 6.00]);
%         
%         
%          
%         file_name= sprintf('./Dataset/room%d/%s/svm_%s_ground.pdf',room,skel_file{(pose)},skel_file{(pose)});
%         print(gcf, '-dpdf', file_name); 
         
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%       
  % draw skeletons
  
  end
  
  file_name= sprintf('./Dataset/room%d/%s/svm_imbalance.mat',room,skel_file{(pose)});
  imbalance.Fscore=Fscore
  imbalance.FP=FP_sum
  imbalance.FN=FN_sum
  imbalance.TP=TP_sum
  save(file_name,'imbalance');
  
  end
end
  keyboard
  
           


          mapfile = sprintf('./Dataset/room%d/map.mat',room);  
        
          load(mapfile,'map');   
                  
          center=map.center;
          
          
     YG_name=sprintf('./Dataset/room%d/%s/YGround.mat',room,skel_file{affordance{affordance_type}(pose)});
     load(YG_name,'ymax');     
          

          Zheight =ymax(3)/10-center(3)+50-1;


        
       
        Torso_mat =Torso_mat+repmat([0, 0, Zheight,0 ],length(Torso_mat),1);   
        
%         map_mesh=reshape(prob_all,[189,101*101]);
%         map_mesh=reshape(sum(map_mesh),[101,101]); 
            
            
           
            
  %   [ B,I]= sort(predictions,'descend');
            
        I= find(predictions>0)  ;  
   %  I=I(1:100);      
 
 LLL=length(I)
 
 %keyboard
 Sit= Torso_mat(I',:);
Sit_tra= (Sit(:,1:3)+repmat([-50+center(1) -50+center(2) -50+center(3)],size(Sit,1),1))*10;
 Sit_tra= [Sit_tra,Sit(:,4)] ;   

            
            
            
            
              
 yymax=Sit_tra;

 

 Vskel_file=sprintf('./Dataset/room%d/%s/Vskel.mat',room,skel_file{affordance{affordance_type}(pose)}); 


Vskel2=load(Vskel_file);


Vskel3 =Vskel2.Vskel1;
 
 
 
global Vskel;
Vskel=Vskel3;
global mapresolution;
global skelresolution;
mapresolution=1;
skelresolution=1;
%x=[ 564.7350  187.4020  231.0440    5.1000] % ground truth

%xnew =[483.5000  219.6990  231.0440    4.1000];
%drawskeleton(xnew);
length(yymax)

skelfile=sprintf('./Dataset/room%d/%s/infer.pcd',room,skel_file{affordance{affordance_type}(pose)});   

if length(yymax)<20
drawskeletonAll(yymax,skelfile);


else
drawskeletonAll(yymax(1:round(length(yymax)/20):length(yymax),:),skelfile);
    
end   
  
  
  
  
 
    
    
   
   keyboard

%            
           
          
       
%         map_mesh_neg=reshape(prob_all_neg,[189,101*101]);
%         map_mesh_neg=reshape(max(map_mesh_neg),[101,101]);
%         
%         map_mesh_pos=reshape(prob_all_pos,[189,101*101]);
%         map_mesh_pos=reshape(max(map_mesh_pos),[101,101]);
%         
        map_mesh_oc=reshape(oc_sum,[189,101*101]);
        map_mesh_oc=reshape(max(map_mesh_oc),[101,101]);
%         
        map_like_pos=reshape(like_pos,[189,101*101]);
        map_like_pos=reshape(max(map_like_pos),[101,101]);
%         
        map_like_neg=reshape(like_neg,[189,101*101]);
        map_like_neg=reshape(max(map_like_neg),[101,101]);
%         
         map_diff_all=reshape(prob_diff,[189,101*101]);
         map_diff_all=reshape(max(map_diff_all),[101,101]);
        
         ground_mesh=reshape(class_lab,[189,101*101]);
         ground_mesh=reshape(max(ground_mesh),[101,101]);
        
        
         map_mesh=reshape(prob_all,[189,101*101]);
         map_mesh=reshape(sum( map_mesh),[101,101]);
         
         
         
        
         
         
         
         C=drawroom(room);

        
         
        

         
        % create new figure
         
         subplot(3,2,2) % first subplot
         image(C);
         
         hold on
         ground_ind = find(map_mesh>0);
         
         s = [101,101];
         [ground_x,ground_y] = ind2sub(s,ground_ind);      
                  
         scatter(ground_x*10,ground_y*10,50,map_mesh(ground_ind),'filled');
         title('Classification');
         
         
         
        
         
        subplot(3,2,1) % first subplot
         image(C);
         hold on
         
         ground_ind = find(ground_mesh>0);
         
         s = [101,101];
         [ground_x,ground_y] = ind2sub(s,ground_ind); 
          
       
         
        scatter(ground_x*10,ground_y*10,50,'y','filled');
         title('Ground Truth');
         
         
         
         
        
         subplot(3,2,3) % first subplot
%          image(C);
%          hold on
%          ground_ind = find (map_like_pos>0);
%         % caxis([0.5 1])
%          s = [101,101];
%          [ground_x,ground_y] = ind2sub(s,ground_ind); 
%          
%          scatter(ground_x*10,ground_y*10,50, map_like_pos(ground_ind)*10,'filled');
         contourf((map_like_pos));
         title('map max');
         
         
         
         
          subplot(2,2,4) % first subplot
%          image(C);
%            hold on
%           ground_ind = find ( map_mesh_oc>0);
% %          
%          s = [101,101];
%          [ground_x,ground_y] = ind2sub(s,ground_ind);      
%          
%        %  contourf(1:10:1010,1:10:1010,map_like_neg);
%          
%         scatter(ground_x*10,ground_y*10,50, map_mesh_oc(ground_ind),'filled');
         contourf((map_like_neg))
         title('Like neg');
         
         
         subplot(3,2,5) % first subplot
%         subplot(2,2,4) % first subplot
%          image(C);
%            hold on
%           ground_ind = find ( map_mesh_oc>0);
% %          
%          s = [101,101];
%          [ground_x,ground_y] = ind2sub(s,ground_ind);      
%          
%        %  contourf(1:10:1010,1:10:1010,map_like_neg);
%          
%         scatter(ground_x*10,ground_y*10,50, map_mesh_oc(ground_ind),'filled');
         contourf((map_diff_all))
         title('Like Diff');
        
         
         
         
         
%          subplot(2,3,4) % first subplot
%          image(C);
%          hold on
%          scatter(ground_x*10,ground_y*10,50,map_mesh(ground_ind),'filled');
%          title('occupied');
%          
%          subplot(4,2,5) % first subplot
%          image(C);
%          hold on
%          scatter(ground_x*10,ground_y*10,50,map_mesh(ground_ind),'filled');
%          title('like pos');
%          
%          subplot(4,2,6) % first subplot
%          contourf(map_like_neg);
%          title('like neg');
%          
%          subplot(4,2,7) % first subplot
%         image(C);
%          hold on
%          scatter(ground_x*10,ground_y*10,50,map_mesh(ground_ind),'filled');
%          title('like diff');
%          
%          subplot(4,2,8) % first subplot
%          image(C);
%          hold on
%          scatter(ground_x*10,ground_y*10,50,map_mesh(ground_ind),'filled');
%          title('feature');
%          
%          
         plot_file=sprintf('./Dataset/room%d/%s/plot.fig',room,skel_file{affordance{affordance_type}(pose)}); 
         savefig(plot_file);
         
        
         
   %   system('pkill -u jmpiyath')
    
         
    
    
             
            
            skel_mat=sprintf('./full_mat/skel_points_%s.mat',skel_file{affordance{affordance_type}(pose)});   

           load(skel_mat,'full_mat');

            Torso_mat =full_mat(3:P_length:length(full_mat),:);
                    
           full_mat=[];
           


          mapfile = sprintf('./Dataset/room%d/map.mat',room);  
        
          load(mapfile,'map');   
                  
          center=map.center;

          Zheight =meanAll(3)/10-center(3)+50-1;


        
       
        Torso_mat =Torso_mat+repmat([0, 0, Zheight,0 ],length(Torso_mat),1);   
        
%         map_mesh=reshape(prob_all,[189,101*101]);
%         map_mesh=reshape(sum(map_mesh),[101,101]); 
            
            
           
            
     [ B,I]= sort(prob_diff,'descend');
            
            
     I=I(1:100);      
 
 LLL=length(I)
 
 %keyboard
 Sit= Torso_mat(I',:);
Sit_tra= (Sit(:,1:3)+repmat([-50+center(1) -50+center(2) -50+center(3)],size(Sit,1),1))*10;
 Sit_tra= [Sit_tra,Sit(:,4)] ;   

            
            
            
            
              
 yymax=Sit_tra;

 skel_mat_points=sprintf('./Dataset/room%d/%s/skel_points.mat',room,skel_file{affordance{affordance_type}(pose)});   

 Vskel2=load(skel_mat_points);


 Vskel3 =Vskel2.Vskel1;

 
 
 
 
global Vskel;
Vskel=Vskel3;
global mapresolution;
global skelresolution;
mapresolution=1;
skelresolution=1;
%x=[ 564.7350  187.4020  231.0440    5.1000] % ground truth

%xnew =[483.5000  219.6990  231.0440    4.1000];
%drawskeleton(xnew);
length(yymax)

skelfile=sprintf('./Dataset/room%d/%s/infer.pcd',room,skel_file{affordance{affordance_type}(pose)});   

if length(yymax)<20
drawskeletonAll(yymax,skelfile);


else
drawskeletonAll(yymax(1:round(length(yymax)/20):length(yymax),:),skelfile);
    
end   
              
         II= prob_all';
         
         a= sum(class_lab .* II);
         c=  sum(~class_lab .* II);
         b= sum(class_lab .* ~II);
         d= sum(~class_lab .*~II);
         
         Fscore= 2*a/(2*a+b+c)
         
         
         performance =[a,b,c,d, a/(a+b),b/(a+b) Fscore];
         
         perfor_file=sprintf('./Dataset/room%d/%s/perfor.mat',room,skel_file{affordance{affordance_type}(pose)});   
         
         
         save( perfor_file,'performance'); 
              
         
                
        
     
        
        
          
             
    

        
        
         
         
        
  
  
  
  
  
    

  
  
 
  
 
  

      
      
      
      







  
  
  
  
