


function [Beta_score]= opti_thersh(max)

affor_name =['relaxing'];
affor_name =cellstr(affor_name);
affor_num = 1;
n=1;
ff=[1,2,3,4,5,6];



prob_truth_file=  sprintf('./Dataset/living%d/%s/prob_truth_all.mat',ff(n),char(affor_name(affor_num)));
load(prob_truth_file,'prob_all');

ground_truth_file=  sprintf('./Dataset/living%d/%s/ground_truth_all.mat',ff(n),char(affor_name(affor_num)));
load(ground_truth_file,'class_lab');

probmax_I= [prob_all>max]';



 a= sum(probmax_I .* class_lab);
 b= sum(probmax_I .* ~class_lab);
 c= sum(~probmax_I .* class_lab);
 Beta_score= 2*a /(2*a +b+c);

 
Beta_score =100*(1-Beta_score)

end