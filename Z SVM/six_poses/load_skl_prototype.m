function [poses] = load_skl_prototype(posefile, supportfile)
global SKL_POSE_NUM

% poses is a vertical vector of structure {'joints', 'support_joints'}
% 'joints' is a JOINT_NUM x 3 matrix. Each row is the (x,y,z) of joint i
% 'support_joints' is a JOINT_NUM x 1 binary vector, indicating whether the
%       joint i is a support of this pose

poses = cell(SKL_POSE_NUM,1);
p = load(posefile);
for i = 1:size(p,1)
    % row format: pose_id joint1 joint2, ...    
    if (p(i,1)<=SKL_POSE_NUM)
        poses{p(i,1)}.joints = reshape(p(i,2:end),3,[])';
    end
end
su = load(supportfile);
for i = 1:size(su,1)
    % row format: pose_id s1 s2 ...
    if (su(i,1)<=SKL_POSE_NUM)
        poses{su(i,1)}.support_joints = su(i,2:end)';
    end
end

end
