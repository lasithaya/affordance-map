function  [map_gridded] = skel_map( A)
%UNTITLED3 Summary of this function goes here
%   resolution

global mapresolution 
samples =10*mapresolution;

mapgrid =zeros(samples,samples,samples);


k=length(A);


NewA =round( A+repmat(([5*mapresolution,5*mapresolution,5*mapresolution]),k,1));


hold on;

 %h=scatter3(NewA(:,1),NewA(:,2),NewA(:,3));
 


% for xx=1:samples
% 
% for yy=1:samples
%     
% for zz=1:samples  
%      
% NewA =A-repmat(([xx-50;yy-50;zz-50])',k,1);
% mincam =sqrt(min(sum(NewA.^2, 2)));
% 
% 
% if mincam<1
%  
% mincam=mincam*1
%     
% end
% 
% mapgrid(xx,yy,zz)= mincam; 
% 
% end 
% end




for j=1:k
 
   if (NewA(j,1)<10*mapresolution && NewA(j,1)>0 && NewA(j,2)<10*mapresolution && NewA(j,2)>0 &&  NewA(j,3)<10*mapresolution && NewA(j,3)>0) 
    
    
   mapgrid(NewA(j,1),NewA(j,2),NewA(j,3))=1;


   end
end



map_gridded =(mapgrid);
    

end

