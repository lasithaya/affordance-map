
skel_file =cellstr(['reaching';'sitting ';'standing';'relaxing';'leaning ';'working ']);

affordance_type=1;

affordance{1} =[4,5,6]; %sitting
affordance_name =cellstr(['sitting']);

format long;


for room=1:1
    ff=[1,2,3,4,5,6];
    
    
    ff(room)=[];
    k=0;
         
    for inn= 1:6
    
    postive_features{inn}=[];
    negative_features{inn}=[];
    postive_features_new{inn}=[];
    negative_features_new{inn}=[];
    
    end
     
        for n=1:5
            n
         
         for  pose=1:size(affordance{affordance_type},2)  
             
           skel_mat=sprintf('./Dataset/room%d/%s/skel_points.mat',ff(n),skel_file{affordance{affordance_type}(pose)});   


           %Vskel2=load('Vskel.mat');
           Vskel2=load(skel_mat);


          Vskel1 =Vskel2.Vskel1;

          P_length= length(Vskel1);
             
         
          mapfile = sprintf('./Dataset/room%d/map.mat',ff(n));  
        
          load(mapfile,'map');   
          min_room =map.minroom/10;
          max_room=map.maxroom/10;
          center=map.center;
             
          skel_mat=sprintf('./full_mat/skel_points_%s.mat',skel_file{affordance{affordance_type}(pose)});   

          load(skel_mat,'full_mat');
                  
            
          
          
          
         Torso_mat =full_mat(3:P_length:length(full_mat),:);
         full_mat=[];
         Torso_mat= (Torso_mat(:,1:3)+repmat([-50+center(1) -50+center(2) -50+center(3)],size(Torso_mat,1),1));
         
         groundlabel_mat=sprintf('./Dataset/room%d/%s/class_lab.mat',ff(n),skel_file{affordance{affordance_type}(pose)});   
         load(groundlabel_mat,'class_lab');
         
         
         neg_index =  ( Torso_mat(:,1)<=max_room(1) & Torso_mat(:,1) >= min_room(1) & Torso_mat(:,2)<=max_room(2) & Torso_mat(:,2) >= min_room(2) & ~class_lab);
             
          
          
         feature_name=sprintf('./Dataset/room%d/%s/feature.mat',ff(n),skel_file{affordance{affordance_type}(pose)});  % load file
         feature_name_oc=sprintf('./Dataset/room%d/%s/feature_oc.mat',ff(n),skel_file{affordance{affordance_type}(pose)});  % load file
    
         
         features=load(feature_name,'features');
         features=features.features;
         features =features(1:15,:);
         
           ll=features < 2;
% 
         features =double( abs(features.*ll-ll*2));
         
         
        % features=features(1:15,:);
         
         
       % features= exp(-1*features);
%          
%         features_oc=load(feature_name_oc,'features_oc');
%         features=features_oc.features_oc;
%         features=double(features);
%          features= exp(-1*features);
        


         
      positive_features1 =features(:,class_lab');     
         
            
      negative_features1 =features(:,neg_index );
         
      neg_features=   negative_features1(:,randperm(length(negative_features1 ),100));
      pos_features = positive_features1;

      
     
%       neg_features = [datasample(negative_features1',100)]';
%       pos_features =  [datasample(positive_features1',100)]';
%       
%       
      p_B = kde( double ( pos_features), 'LCV' );
      p_neg_B= kde( double(neg_features), 'LCV' );
         
      Neg_band=getBW( p_neg_B,1);
      Pos_band =getBW( p_B,1);
      
      
      
     % pos_features=getPoints(resample(p_B,100,'ROT'));
      
     % neg_features=getPoints(resample(p_neg_B,100,'ROT'));
      
      
         
      Weights{1} =ones(1,length(class_lab));
         
      pos_weights2 = ones(1,size(pos_features,2))/size(pos_features,2);
      neg_weights2 = ones(1,100)/100;
      
         
       Pos_like_prev=0;
         
         
         for g=1:20
         
        
       
         
         
         p_B = kde( (double (pos_features(:,:))),  Pos_band ,pos_weights2);
         p_neg_B= kde( (double(neg_features(:,:))),  Neg_band ,neg_weights2 );
         
         
        
         P_length= size(features,1);
           
           
           
           
         multi=101*101*3;
         ang_multi=63;
         
         
         
         
         
         
           TTT=mat2cell(features,P_length,101*101*3*ones(1,63));
           tic
           
            parfor ii=1: ang_multi
           
                prob_all_pos_cell{ii}=evaluate(p_B,(TTT{1,ii}));
                prob_all_neg_cell{ii}=evaluate(p_neg_B,(TTT{1,ii}));
           % ii
            
            end
           
           
          toc
          prob_all_pos= cell2mat(prob_all_pos_cell);
           
          prob_all_neg= cell2mat(prob_all_neg_cell);
          
          
          
          prob_al_pos(prob_all_pos==0)=realmin;
          prob_al_neg(prob_all_neg==0)=realmin;
           
         
          
          
          Pos_like_diff =(log(prob_all_pos*sum(pos_weights2))-log(prob_all_neg)*sum(neg_weights2))/2;
          
          
          Pred_pos =  (Pos_like_diff+Pos_like_prev)>0;
          
          Pos_like_prev=Pos_like_diff ;
          
          
          Neg_like_diff =Pos_like_diff*(-1);
          
          Pred_neg = ~Pred_pos;          
          
          Weights{g+1}= Weights{g}.*(class_lab'.*exp(Neg_like_diff*(1))+~class_lab'.*exp(Pos_like_diff)*(1));
           
          Weights{g+1}= Weights{g+1}/sum( Weights{g+1});
         
         FP=  and(~class_lab' ,Pred_pos );
         FN=  and(class_lab' , Pred_neg); 
         
         TP = and(class_lab' ,Pred_pos );
         
         [ B_POS,I_POS]= sort(Weights{g+1}(class_lab'),'descend');
         [ B_Neg,I_Neg]= sort(Weights{g+1}(~class_lab'),'descend');
         
         Index_FP=find(class_lab');
         Index_FN=find(~class_lab');
         
         FP_sum = sum(FP)
         FN_sum = sum(FN)
         TP_sum = sum(TP)
         
         
         Fscoe = 2*TP_sum /(2*TP_sum +FP_sum+FN_sum)
        
         
          pos_features = features(:, Index_FP(I_POS));
          pos_weights =  Weights{g+1}(Index_FP(I_POS));
         
        
        
         neg_features = features(:, Index_FN(I_Neg(1:100)));
         neg_weights =  Weights{g+1}(Index_FN(I_Neg(1:100)));
        
          pos_weights2 =  pos_weights/(sum(pos_weights)+sum(neg_weights));
          neg_weights2 =  neg_weights/(sum(pos_weights)+sum(neg_weights));
         
%         neg_features = [neg_features,New_pos_features];
%         pos_features=[pos_features,New_neg_features];
           
          
          end 
          keyboard
        
        
         postive_features{affordance{affordance_type}(pose)} =[postive_features{affordance{affordance_type}(pose)}, pos_features,New_pos_features];
         
         negative_features{affordance{affordance_type}(pose)}=[ negative_features{affordance{affordance_type}(pose)}, neg_features,New_neg_features];
         
%          postive_features_new{affordance{affordance_type}(pose)} =[postive_features_new{affordance{affordance_type}(pose)}, New_pos_features];
%          
%         negative_features_new{affordance{affordance_type}(pose)}=[ negative_features_new{affordance{affordance_type}(pose)},New_neg_features];
        
        
         end
        
        
          
        end
 
        
        
        
        for tt=1:6
            
          if (size(postive_features{tt},1)>0)
            
         model_name_pos=sprintf('./Dataset/room%d/%s_pos.mat',room,skel_file{tt}) ;% load file
         model_name_neg=sprintf('./Dataset/room%d/%s_neg.mat',room,skel_file{tt}) ;% load file
         
         model_name_pos2=sprintf('./Dataset/room%d/%s_pos2.mat',room,skel_file{tt}) ;% load file
         model_name_neg2=sprintf('./Dataset/room%d/%s_neg2.mat',room,skel_file{tt}) ;% load file
        
        % postive_features2= [datasample(postive_features{tt}',500)]';
        % negative_features2=[datasample(negative_features{tt}',500)]';
         
         postive_features2= postive_features{tt};
         negative_features2=negative_features{tt};
        postive_features2_new= postive_features_new{tt};
        negative_features2_new= negative_features_new{tt};
         p = kde(  (postive_features2), 'ROT' );
         p_neg= kde( (negative_features2), 'ROT' );
         
%          p2 = kde( double (postive_features2_new), 'ROT' );
%          p_neg2= kde( double(negative_features2_new), 'ROT' );
         
         save(model_name_pos,'p');
         save(model_name_neg,'p_neg')
%          save(model_name_pos2,'p2');
%          save(model_name_neg2,'p_neg2')
         
         
         
         
            end
        end
          
end

  
 
 


  
  




  
 
  
  
  
  
  
  
  
  
  
  
  
  
  