ff=[1,3,4,5,6,7];

k=2;

ccfile = sprintf('./Dataset/living%d/cc.mat',ff(k));  
load(ccfile,'cc');

LLfile = sprintf('./Dataset/living%d/LL.mat',ff(k));  
load(LLfile,'LL'); 

mapfile = sprintf('./Dataset/living%d/map.mat',ff(k));  
        
load(mapfile,'map'); 

YG_name=sprintf('./Dataset/living%d/YGround.mat',ff(k));  % load groundtruth
    
load(YG_name,'ymax'); % load ground truth
     
y = ymax;
    



Zheight=map.Zheight;
[v,ind2]=max(cc(:));


 t=0:0.02:1;
 Precesion=zeros(1,length(t));
 Recall = zeros(1,length(t));
 for k=1:length(t)

        ind =find(cc(:)>=v-t(k));

        %[x11,y11,zz11,theta11] = ind2sub(size(cc),ind);
        %zzz= Zheight*ones(length(x11),1)+zz(zz11)';

        %yymax =[xx(x11)',yy(y11)',zzz,theta(theta11)'];

        lossmin=LL(ind);

        FP=sum(lossmin(:)==0);  
        TP=sum(lossmin(:)~=0);
        

        FN=1*(size(y,1)-sum(unique(lossmin)~=0));

        Precesion(k) = TP/(TP+FP);
        Recall(k) =TP/(TP+FN);
 end

 plot(Recall,Precesion);
 