skel_file =cellstr(['relaxing';'leaning ';'working ']);
global Vskel;
for ff=1:8

for poses=1:3
    
    
Vskel_file=sprintf('./Dataset/room%d/%s/Vskel.mat',ff,skel_file{poses}); 
skel_pcd =sprintf('./Dataset/room%d/%s/skel_base.pcd',ff,skel_file{poses});
load('./six_poses/skeltonall.mat');
Vskel1 =(reshape(joints(poses+3,:),3,15) )/10;
Vskel1=Vskel1-repmat(Vskel1(:,3),1,15) ;  
Vskel1=[Vskel1(1,:);Vskel1(2,:);Vskel1(3,:);];
  
  % in cm 
save (Vskel_file,'Vskel1');



Vskel=Vskel1;
global mapresolution;
global skelresolution;
mapresolution=1;
skelresolution=1;
x=[ 0  0  0   0] ;% ground truth

%xnew =[483.5000  219.6990  231.0440    4.1000];
%drawskeleton(xnew);
drawskeletonAll(x,skel_pcd);












end

end