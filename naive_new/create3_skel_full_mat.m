x= 0:10:1000;
y= 0:10: 1000;
theta=0:0.1:2*pi;
z=0:10:20;
skel_file =cellstr(['relaxing';'working ';'leaning ']);

for ff=1:1

for poses=3:3
    poses

skel_mat=sprintf('./Dataset/room%d/%s/skel_points.mat',ff,skel_file{poses});   
skel_mat_save=sprintf('./full_mat/skel_points_%s.mat',skel_file{poses});   
    


%Vskel2=load('Vskel.mat');
Vskel2=load(skel_mat);


Vskel1 =Vskel2.Vskel1;

full_mat =zeros(length(x)*length(y)*length(z)*length(theta)*length(Vskel1),4);


SkelG2 = Vskel1/10;

c=0;

for i=1:length(x)
  i
   for j=1:length(y)
         for n=1:length(z)
                for k=1:length(theta)
           
                       start =c+1;
                       c =start+length(Vskel1)-1;
                      eend =c;
               
                        xx=x(i)/10;
                        yy=y(j)/10;
                        zz=z(n)/10;
                        angle =theta(k);
                        rotmat=rotation(angle);
                        

                       
                        

                        % new tranformed position


                        New_skel1 =(rotmat*SkelG2)'+repmat([xx yy zz],length(SkelG2'),1);
                        
                        New_skel2 =[New_skel1, ones(length(Vskel1),1)*angle];
                        full_mat(start:eend,:) =[New_skel2];
                      
                        
                end
        end
   end
end

save (skel_mat_save,'full_mat');

end
end
