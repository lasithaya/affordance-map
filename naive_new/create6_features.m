
clear
skel_file =cellstr(['reaching';'sitting ';'standing';'relaxing';'leaning ';'working ']);


load('skel_points.mat')
Size_skel= length(Vskel1);
poses_array =[4,6,5];
room_array=[5,6,8];

for rr=1:3
   room=room_array(rr);
    for ff=3:3
    poses=poses_array(ff);
    skel_mat=sprintf('./Dataset/room%d/%s/skel_points.mat',room,skel_file{poses}); 
    load(skel_mat);
    Size_skel= length(Vskel1);
    
    
    feature_name=sprintf('./Dataset/room%d/%s/feature.mat',room,skel_file{poses});  % load file
    feature_name_oc=sprintf('./Dataset/room%d/%s/feature_oc.mat',room,skel_file{poses});  % load file
    
     
            mapfile = sprintf('./Dataset/room%d/map.mat',room);  
        
            load(mapfile,'map');   
                  
            DM=map.dis;
            ocmap=map.oc;
            center=map.center;
            
            YG_name=sprintf('./Dataset/room%d/%s/YGround.mat',room,skel_file{poses});
            load(YG_name,'ymax');
            meanAll =(ymax);
            
            
            Zheight =meanAll(3)/10-center(3)+50-1;
            
        %    features=single(zeros(15, 101*101*63*3));
            
     mapfile=sprintf('./full_mat/skel_points_%s.mat',skel_file{poses});        

     

  
  
  
 full_mat=load(mapfile,'full_mat');
 
  full_mat=full_mat.full_mat;

  full_mat =single(full_mat(:,1:3))+repmat([0, 0, Zheight ],length(full_mat),1);
  
  
  tic
  dis =(DM(full_mat));
  features =reshape(dis,Size_skel,length(dis)/Size_skel);
  save(feature_name,'features');
  features=[];
  oc=int8(ocmap(full_mat));
  
%   dis1  =double(DM(full_mat(1:length(full_mat)/2,1:3)));
%   dis2  =double(DM(full_mat(length(full_mat)/2+1:length(full_mat),1:3)));
%   toc
 % clearvars 'full_mat';
 
%   dis=[dis1;dis2];
%  
%    clearvars 'dis1';
%    clearvars 'dis2';
  
  features_oc =reshape(oc,Size_skel,length(dis)/Size_skel);
  
  
  
 % features(tt*100+1:tt*100+100,:)=sign(reshape(dis,100,length(dis)/100));
%  clearvars 'dis';
%    Dis_shape =dis_check;
%  clearvars 'full_mat';

  
 
  save(feature_name_oc,'features_oc');        
            
            
       features_oc=[];      
            
            
            
            
            
    end           
            
            
end