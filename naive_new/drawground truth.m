 ff=[1,3,4,5,6,7];

for n=1:5
            
     
            mapfile = sprintf('./Dataset/living%d/map.mat',ff(n));  
        
            load(mapfile,'map');   

            patterns{k}=map;% input 3d distance transform of the 3d image
            
            DM=map.dis;
            center=map.center;
            Zheight =map.Zheight/10-center(3)+50-1;
            

            load('full_mat.mat','full_mat');

            full_mat =full_mat+repmat([0, 0, Zheight,0 ],length(full_mat),1);
            Torso_mat =full_mat(3:15:length(full_mat),:);
            legA =full_mat(14:15:length(full_mat),:);
            legB = full_mat(15:15:length(full_mat),:);
            HipA =full_mat(8:15:length(full_mat),:);
            HipB =full_mat(10:15:length(full_mat),:);
           % dis  =DM(full_mat(:,1:3));

           
            sit_file = sprintf('./Dataset/living%d/sitting.mat',ff(n)); 
            leg_file = sprintf('./Dataset/living%d/leg.mat',ff(n)); 
            load(sit_file,'sitting');
            load(leg_file,'leg');

% transform ground truth  leg polygon and hip polygon to dis map



% coordinates
          class_lab=zeros(length(Torso_mat),1);
          for ss=1:size(sitting,2)
          leg_t = leg{ss}/10 +repmat([50-center(1) 50-center(2)],size(leg{ss},1),1);
          sitting_t = sitting{ss}/10+repmat([50-center(1) 50-center(2)],size(sitting{ss},1),1);

          hips_inA = inpolygon(HipA(:,1),HipA(:,2),sitting_t(:,1),sitting_t(:,2));
          hips_inB = inpolygon(HipB(:,1),HipB(:,2),sitting_t(:,1),sitting_t(:,2));

         leg_inA= inpolygon(legA(:,1),legA(:,2),leg_t(:,1),leg_t(:,2));
         leg_inB= inpolygon(legB(:,1),legB(:,2),leg_t(:,1),leg_t(:,2));


         All_sitting = (hips_inA+hips_inB+leg_inA+leg_inB);
         Issit =(All_sitting==4);

         class_lab =or (class_lab,and((Torso_mat(:,3)==Zheight+1),Issit));
         
          end
         
         lab = class_lab*2-ones(length(class_lab),1);
           keyboard                
         
         labels{k} =lab;
          end