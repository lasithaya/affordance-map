function C= drawroom(k)

plyname=sprintf('./Dataset/room%d/room%dS.ply',k,k);
     
ply=plyread(plyname);

room =[ply.vertex.x,ply.vertex.y];


Red =ply.vertex.red/255;
Green =ply.vertex.green/255;
Blue=ply.vertex.blue/255;


C=ones(1000,1000,3);
% C(:,:,1)=0;
% C(:,:,2)=102/255;
% C(:,:,3)=204/255;


center =(max(room)+min(room))/2;


NewA =round( room+repmat(([500-center(1),500-center(2)]),length(room),1));


for nn=1:length(room)
C(NewA(nn,1),NewA(nn,2),1)=Red(nn);

C(NewA(nn,1),NewA(nn,2),2)=Green(nn);

C(NewA(nn,1),NewA(nn,2),3)=Blue(nn);
end

%image(C);

end