% create translation and rotational grid for each grid

for tt=0:9
mapfile = sprintf('full_mat_%d.mat',tt);  

x= 0:10:1000;
y= 0:10: 1000;
theta=0:0.1:2*pi;
z=0:10:20;


load('skel.mat')

full_mat =int16(zeros(length(x)*length(y)*length(theta)*length(z)*100,3));


SkelG2 = skel(:,tt*100+1:tt*100+100)/10;
%SkelG2=skel/10;

c=0 

for i=1:length(x)
  i
   for j=1:length(y)
        for n=1:length(z)
        
                for k=1:length(theta)
           
                       start =c+1;
                       c =start+99;
                        eend =c;
               
                        xx=x(i)/10;
                        yy=y(j)/10;
                        zz=z(n)/10;
                        angle =theta(k);
                        rotmat=rotation(angle);
                        

                       
                        

                        % new tranformed position


                        New_skel1 =(rotmat*SkelG2)'+repmat([xx yy zz],length(SkelG2'),1);
                        
                       % New_skel2 =[New_skel1, ones(1000,1)*angle];
                         New_skel2= New_skel1;
                        full_mat(start:eend,:) =(New_skel2)*100;
                      
                        
                end
        end
        
   end
end

save (mapfile,'full_mat');
clearvars 'full_mat';

end
