function  [F,map_gridded] = map_dis( A)
%UNTITLED3 Summary of this function goes here
%   resolution

global mapresolution 
samples =100*mapresolution;

mapgrid =zeros(samples,samples,samples);


k=length(A)


NewA =round( A+repmat(([50*mapresolution,50*mapresolution,50*mapresolution]),k,1));
scatter3(NewA(:,1),NewA(:,2),NewA(:,3),'.');
hold on

% for xx=1:samples
% 
% for yy=1:samples
%     
% for zz=1:samples  
%      
% NewA =A-repmat(([xx-50;yy-50;zz-50])',k,1);
% mincam =sqrt(min(sum(NewA.^2, 2)));
% 
% 
% if mincam<1
%  
% mincam=mincam*1
%     
% end
% 
% mapgrid(xx,yy,zz)= mincam; 
% 
% end 
% end




for j=1:k
 
   if (NewA(j,1)<100*mapresolution && NewA(j,1)>0 && NewA(j,2)<100*mapresolution && NewA(j,2)>0 &&  NewA(j,3)<100*mapresolution && NewA(j,3)>0) 
    
    
   mapgrid(NewA(j,1),NewA(j,2),NewA(j,3))=1;


   end
end



mapfill = imfill(mapgrid,'holes');

%D= bwdist(mapfill)-bwdist(~mapfill);
%D= bwdist(mapfill)+mapfill*(10);

new_oc= mapgrid*1+(~mapgrid)*(-1);

D= bwdist(mapgrid);

F = griddedInterpolant(D);
map_gridded =griddedInterpolant(new_oc);
    

end

