function [model]= Learnroom_main_structured
skel_file =cellstr(['relaxing';'leaning ';'working ']);
global pose;
pose=2;
format long;

global room;

    for room=8:8
        
         
    
    
          
          
             
              
         
            % groundlabel_mat=sprintf('./Dataset/room%d/%s/class_lab.mat',ff(n),skel_file{affordance{affordance_type}(pose)});   
            % load(groundlabel_mat,'class_lab');
            
              
    
    
             label_name=sprintf('./Dataset/room%d/%s/class_label_all.mat',room,skel_file{pose}) ;% load file   
             load( label_name,'class_label_all');
            
         
             lab = class_label_all';
                                
          
             labels{1} =lab;       
                    
         
             feature_name=sprintf('./Dataset/room%d/%s/features_all.mat',room,skel_file{pose}) ;% load file 
         
             patterns{1}= feature_name;
          
           

           
    
     parm.patterns = patterns ;
     parm.labels = labels ;
     parm.lossFn = @lossCB ;
     parm.constraintFn  = @constraintCB ;
     parm.featureFn = @featureCB ;
%   parm.endIterationFn = @iterCB ;
     parm.dimension = 2000 ;
     parm.verbose = 1 ;
     model = svm_struct_learn(' -c 100  -v 1 -e 1', parm) ;
 
          
    model_name=sprintf('./Dataset/room%d/%s/model_struct.mat',room,skel_file{pose}) ;% load file   
    save(model_name,'model');
    
     
 
    end 
   
   % system('pkill -u jmpiyath');
    
    
end  
    
 function psi = featureCB(param, x, y)
     
     
    fprintf('feature function \n')  
  
    features=load(x,'features');
    features=features.features;
  
   dim=size( features,1);


  
    features=(features.*repmat(int8(y'),dim,1));
  

     psi =(sparse( double(sum((features),2)))) ;
    
     

    fprintf('********************************************\n')  
    

 end        
        
 
 function delta = lossCB(param, y, ybar)
  
  global room
  a= sum((y+ybar)==2);
  b = sum((y==-1) .* (ybar==1));
  c= sum((y==1) .* (ybar==-1));
   
   
  delta =100*(1-(2*a/(2*a+b+c)))

   room
 
  end
        
 function yhat = constraintCB(param, model, x, y)
   tic; 
   global room
   global pose
   
  fprintf('constrain function\n')    
      
 
    
    
  %  W=rand(1000,1);
  
  
  W=model.w;
  features=load(x,'features');
  features=features.features;
    
    
    
    
  wphi_pos=double(W'*single(features));
  clearvars 'features';
   


  
   
  Pos = find(y==1);
  Neg =find(y==-1);
  
  posW= wphi_pos(Pos);
 
  pos_sum_ALLN=-1*sum(posW);
  
  [posd,Ipos]=sort(posW,'descend');
  
  sorted_pos =Pos(Ipos);
 
  
  negW= wphi_pos(Neg);
  neg_sum = sum(negW);
  
  [pNegd,Ineg]=sort(negW,'descend');
  
 
  
  
  sorted_neg =Neg(Ineg);
 % sorted_neg =[sorted_neg;1];
  
  pos_num=length(Pos)
  
  neg_num =length(Neg)
  

 %prev_max_con=-inf;
 
 max_con_pos_max= (zeros((pos_num+1), 1));
 max_con_pos_a= (zeros((pos_num+1), 1));
 max_con_pos_b= (zeros((pos_num+1), 1));
 
 %max_con_neg= (zeros((neg_num+1) ,3));
  %II_pos=0;
  
 % sorted_neg_rev= sorted_neg(length(sorted_neg):-1:1);  
  w_neg=pNegd((length(pNegd):-1:1));
  w_neg=[0,w_neg];
  w_neg_sum=(cumsum(w_neg));
  
  w_pos =[0,posd];
  w_pos_sum=cumsum(w_pos);

  
   parfor a=0:pos_num
   
   c=pos_num-a;
  % pos_sum =pos_sum+abs(2*wphi_pos(sorted_pos(a+1)));
  % Y_star(sorted_pos(a+1))=1;
   pos_sum =  pos_sum_ALLN+(2*w_pos_sum(a+1));
   
  % II_neg=0;
  % loop_sum_neg = neg_sum; 
   
   oneV=ones(1,length(w_neg_sum));
   bI=(neg_num:-1:0);
 % max_neg= oneV*(pos_sum +neg_sum+100)-w_neg_sum*2-100*((2*a*oneV)./ ( (2*a+c)*oneV+bI));
   prec =oneV*100-100*((2*a*oneV)./ ( (2*a+c)*oneV+bI));
  
   neg_sum_all = oneV*neg_sum-(w_neg_sum*2);
  
   pos_sum_all =oneV*pos_sum;
   max_neg=(pos_sum_all+neg_sum_all)+prec;
   
 
    
   [max_con_pos_max(a+1),Ind]= max(max_neg);
   max_con_pos_a(a+1) =a;
   max_con_pos_b(a+1)= neg_num-Ind+1;
   
 
     
   end  
  
 
   toc;
    
   % keyboard;
    [MM,Ind]= max(max_con_pos_max)
    a_max= max_con_pos_a(Ind)
    b_max=max_con_pos_b(Ind)
    
     
     Y_star= ones(length(y),1)*-1;
     
     if a_max>0
     Y_star(sorted_pos(1:a_max))=1;
     end
     
     if b_max>0
     Y_star(sorted_neg(1:b_max))=1;
     end       
   
     
   yhat= Y_star;

skel_file =cellstr(['relaxing';'leaning ';'working ']);

model_name=sprintf('./Dataset/room%d/%s/model_struct.mat',room,skel_file{pose}) ;% load file   
save(model_name,'model');


fprintf('********************************************\n')     
        
 end
        
        
        



  
 
 


  
  




  
 
  
  
  
  
  
  
  
  
  
  
  
  
  