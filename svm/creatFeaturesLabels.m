clear
skel_file =cellstr(['relaxing';'leaning ';'working ']);

% affordance_type=1;
% 
% affordance{1} =[4,6]; %sitting
% affordance_name =cellstr(['sitting']);

format long;
pose=1;
%num_room=4;
room_array{1}=[1,2];
room_array{2}=[5,8,9];
room_array{3}=[3,4,5,6,7];





for rr=1:size(room_array{pose},2)
     
    room=room_array{pose}(rr)
    ff=room_array{pose};
    room_index= find(ff==room);  
    ff(room_index)=[];
    k=0;
         
    
    
     pos_features=int8([]);
     neg_features=int8([]); 
    
    class_lab_all=[];
    
    
    
    class_multi=100;
    
    
    
    for n=1:size(ff,2)
          
           
         
         % n
             
           skel_mat=sprintf('./Dataset/room%d/%s/skel_points.mat',ff(n),skel_file{pose});   
% 
% 
           %Vskel2=load('Vskel.mat');
          Vskel2=load(skel_mat);


          Vskel1 =Vskel2.Vskel1;

          P_length= length(Vskel1);
             
%          
          mapfile = sprintf('./Dataset/room%d/map.mat',ff(n));  
%         
          load(mapfile,'map');   
          min_room =map.minroom/10;
          max_room=map.maxroom/10;
          center=map.center;
             
          skel_mat=sprintf('./full_mat/skel_points_%s.mat',skel_file{pose});   

          load(skel_mat,'full_mat');
                  
            
          
          
          
         Torso_mat =full_mat(3:P_length:length(full_mat),:);
         full_mat=[];
         Torso_mat= (Torso_mat(:,1:3)+repmat([-50+center(1) -50+center(2) -50+center(3)],size(Torso_mat,1),1));
         
         groundlabel_mat=sprintf('./Dataset/room%d/%s/class_lab.mat',ff(n),skel_file{pose});   
         load(groundlabel_mat,'class_lab');
         
        
        
         
         
        % neg_index=  ( Torso_mat(:,1)<=max_room(1) & Torso_mat(:,1) >= min_room(1) & Torso_mat(:,2)<=max_room(2) & Torso_mat(:,2) >= min_room(2) & ~class_lab);
           neg_index=  (~class_lab);  
        
         % feature_name=sprintf('./Dataset/room%d/%s/feature.mat',ff(n)',skel_file{affordance{affordance_type}(pose)});
        feature_name=sprintf('./Dataset/room%d/%s/features_1000.mat',ff(n),skel_file{pose});
         
         
          ff(n)
      
    
         
         features=load(feature_name,'features');
         features=features.features;
%          
%           ll=features < 2;
% % 
%         features =single( abs(features.*ll-ll*2))*10;
         
         
         features2=(features(:,:));
         features=[]; 
         
         
      
      
      
         
         

          
          
      positive_features1 =features2(:,class_lab');
         
            
      negative_features1 =features2(:,neg_index);
         
      features2=[]; 
      neg_features=( [neg_features,  negative_features1]);
      pos_features =([pos_features, positive_features1]);
          
           
      
          
          

    end
    
   features=[pos_features,neg_features];
   class_label_all=[ones(1,size(pos_features,2)),ones(1,size(neg_features,2))*-1];
    
   feature_name=sprintf('./Dataset/room%d/%s/features_all.mat',room,skel_file{pose}) ;% load file   
   save(feature_name,'features');
    
   label_name=sprintf('./Dataset/room%d/%s/class_label_all.mat',room,skel_file{pose}) ;% load file   
   save( label_name,'class_label_all');


         
      
     keyboard    
   
         

        
        
           
          
    end 