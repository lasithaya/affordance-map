

% this creates ground truth data for the sitting and relaxing pose
skel_file =cellstr(['relaxing';'leaning ';'working ']);

global Vskel;



for k=9:9
    
 for poses=1:3

    
    

 f_name=sprintf('./Dataset/room%d/%s',k,skel_file{poses});
 D = dir([f_name, '/ss*.ply']);
 
 
 
 if length(D)>0
 


 
plyname=sprintf('./Dataset/room%d/%s/ss1.ply',k,skel_file{poses});
     
ply=plyread(plyname);

plymat =[ply.vertex.x,ply.vertex.y,ply.vertex.z];

skel =plymat( length(plymat)-14:length(plymat),:);

skelA=skel-repmat(skel(1,:),15,1);

Vskel_file= sprintf('./Dataset/room%d/%s/Vskel.mat',k,skel_file{poses});

Vskel2=load(Vskel_file);

Vskel2 =Vskel2.Vskel1;

Neck = Vskel2(:,2) ;
Head= Vskel2(:,1) ;
Left_shoulder= Vskel2(:,4) ;
Left_elbow= Vskel2(:,5) ;
Left_hand=Vskel2(:,12) ;
Right_shoulder= Vskel2(:,6) ;
Right_elbow= Vskel2(:,7) ;
Right_hand=Vskel2(:,13) ;
Left_hip =Vskel2(:,8) ;
Left_knee= Vskel2(:,9); 
Left_foot=Vskel2(:,14) ;
Right_hip=Vskel2(:,10);
Right_knee=  Vskel2(:,11) ;
Right_foot= Vskel2(:,15);
Torso = Vskel2(:,3);
Pcloud =[Torso';Neck';Head';Left_shoulder';Left_elbow';Left_hand';Right_shoulder';Right_elbow';Right_hand';Left_hip';Left_knee';Left_foot';Right_hip';Right_knee';Right_foot'];


 b=[];
% 
 for t=0:0.01:2*pi
%     
     Pcloud1 =rotation(t)*Pcloud';
     Diff =(skelA-Pcloud1').*(skelA-Pcloud1');
     DD= sum(Diff(:));
     
    b=[b;DD]  ;
%     
%  
%     
 end
 [mm,I]=min((b));
% 
 angle =I*0.01;
% 
 x= [skel(1,1)  skel(1,2)  skel(1,3) angle];
 ymax=x;
 

 YG_name=sprintf('./Dataset/room%d/%s/YGround.mat',k,skel_file{poses});
 save(YG_name,'ymax');
 
 Vskel= Vskel2;
 
 
SF_name =sprintf('./Dataset/room%d/%s/skelG.pcd',k,skel_file{poses});
 drawskeletonAll(x,SF_name);
 
 
 
 
 
 
 end
 end 
 
end
%  
%  feature_match=getSkelFeatures(x);
%  
%  drawskeleton(x);