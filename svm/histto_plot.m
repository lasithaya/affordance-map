
figure
type='LCV';

pos=double(postive_features{1,4});
neg=double(negative_features{1,4});
torso_p =kde((pos(5,:)),type);
torso_n =kde((neg(5,:)),type);

y=-0.5:0.05:3.0;

f_p=evaluate(torso_p,y,type);

f_n=evaluate(torso_n,y,type);


plot(y,f_p,y,f_n,'LineWidth',1.5);

hold


%histogram(pos(5,:),'Normalization','pdf','BinWidth',0.2);

hold on

% [mu,s,muci,sci] = normfit((pos(5,:)));
% 
% norm = normpdf(y,mu,s);
% 
% plot(y,norm,'LineWidth',1.5);