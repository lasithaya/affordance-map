clear


skel_file =cellstr(['reaching';'sitting ';'standing';'relaxing';'leaning ';'working ']);

affordance_type=1;

affordance{1} =[4,6]; %sitting
affordance_name =cellstr(['sitting']);

%format long;


 for room=4:4
  
    
    
        
           room
         
        pose=1                  
          
%                 feature_name=sprintf('./Dataset/room%d/%s/feature.mat',room,skel_file{affordance{affordance_type}(pose)});  % load file
%                 feature_name_oc=sprintf('./Dataset/room%d/%s/feature_oc.mat',room,skel_file{affordance{affordance_type}(pose)});  % load file
%     
%                 
%                 
%                 
%                 features=load(feature_name,'features');
%                 features=double(features.features);
%                 features=features(:,:);
%                 ll=features < 5;
%  
%                 features =(double( abs(features.*ll-ll*5)));
%         
%                features2=[];
%          
%         for  ll=0:19
%          
%          
%          features2= [features2 ; sum(features(ll*6+1:ll*6+6,:))];
%          
%         end
%          
%          features=features2;
         % features_all{n} =exp( features*-1);
%          features= features(sum(features));
          
          feature_name=sprintf('./Dataset/room%d/features_1000.mat',room);
         
         
          
       
    
         
         features=load(feature_name,'features');
         features=(features.features);
         
         %features2=(features(:,:));
                
        


                 
         
                groundlabel_mat=sprintf('./Dataset/room%d/%s/class_lab.mat',room,skel_file{affordance{affordance_type}(pose)});   
                load(groundlabel_mat,'class_lab');
                
                
                model_name_pos=sprintf('./Dataset/room%d/model_struct.mat',room) ;% load file
                load( model_name_pos,'model');
         
                class_label= (class_lab>0)*1+ (class_lab==0)*-1;
                
                
                
                
                               
           predictions= model.w' * single(features);
                
                 
           
         
                
                
            
                
                
                

%             
       
           
          Pred_pos = ( predictions>0)';
          Pred_neg = ~Pred_pos;
           
          
          pred_mesh=reshape(Pred_pos,[63,101*101]);
         pred_mesh=reshape(max(pred_mesh),[101,101]);
         
         
         ground_truth=reshape(class_lab,[63,101*101]);
         ground_truth=reshape(max(ground_truth),[101,101]);
          
          
           
         FP=  and(~ground_truth , pred_mesh);
         FN=  and(ground_truth , ~ pred_mesh); 
         
         TP = and(ground_truth , pred_mesh );
           
      
         FP_sum = sum(FP(:))
         FN_sum = sum(FN(:))
         TP_sum = sum(TP(:))
         
         
         Fscoe = 2*TP_sum /(2*TP_sum +FP_sum+FN_sum)
    
            
    
         
         
         
       
         C=drawroom(room);
         figure
         subplot(1,3,1)
         
         image(C);
         hold on
         
         ground_ind = find(pred_mesh>0);
         
         s = [101,101];
         [ground_x,ground_y] = ind2sub(s,ground_ind); 
          
       
         
         scatter(ground_x*10,ground_y*10,50,'y','filled');
         title('classification');
         
         
         
         
         subplot(1,3,2)
         
         ground_pred=reshape(predictions,[63,101*101]);
         ground_pred=reshape(max(ground_pred),[101,101]);
         
         ground_pred=flipud(ground_pred');
         contourf(1:101,1:101,ground_pred)
         
          
         subplot(1,3,3)
         
         image(C);
         hold on
         
         ground_ind = find( ground_truth>0);
         
         s = [101,101];
         [ground_x,ground_y] = ind2sub(s,ground_ind); 
          
       
         
         scatter(ground_x*10,ground_y*10,50,'y','filled');
         title('classification');
         
         
       skel_mat=sprintf('./full_mat/skel_points_%s.mat',skel_file{affordance{affordance_type}(pose)});   

        load(skel_mat,'full_mat');
           
        skel_mat=sprintf('./Dataset/room%d/%s/skel_points.mat',room,skel_file{affordance{affordance_type}(pose)});   

    


%Vskel2=load('Vskel.mat');
        Vskel2=load(skel_mat);


        Vskel1 =Vskel2.Vskel1;

        P_length= length(Vskel1);
   
           
           
           
           
          
           Torso_mat =full_mat(3:P_length:length(full_mat),:);
                    
           full_mat=[];
           


          mapfile = sprintf('./Dataset/room%d/map.mat',room);  
        
          load(mapfile,'map');   
                  
          center=map.center;
          
          YG_name=sprintf('./Dataset/room%d/%s/YGround.mat',room,skel_file{affordance{affordance_type}(pose)});
          load(YG_name,'ymax');
          meanAll =(ymax);
            
          

          Zheight =meanAll(3)/10-center(3)+50-1;


        
       
        Torso_mat =Torso_mat+repmat([0, 0, Zheight,0 ],length(Torso_mat),1);    
         
        Sit= Torso_mat(Pred_pos,:);
        Sit_tra= (Sit(:,1:3)+repmat([-50+center(1) -50+center(2) -50+center(3)],size(Sit,1),1))*10;
        Sit_tra= [Sit_tra,Sit(:,4)] ;   

            
            
            
            
              
        yymax=Sit_tra;

 Vskel_file=sprintf('./Dataset/room%d/%s/Vskel.mat',room,skel_file{affordance{affordance_type}(pose)}); 


Vskel2=load(Vskel_file);


Vskel3 =Vskel2.Vskel1;
global Vskel;
Vskel=Vskel3;
global mapresolution;
global skelresolution;
mapresolution=1;
skelresolution=1;     

 
 
 
 

%x=[ 564.7350  187.4020  231.0440    5.1000] % ground truth

%xnew =[483.5000  219.6990  231.0440    4.1000];
%drawskeleton(xnew);
length(yymax)

skelfile=sprintf('./Dataset/room%d/%s/infer.pcd',room,skel_file{affordance{affordance_type}(pose)});   

if length(yymax)<20
drawskeletonAll(yymax,skelfile);


else
drawskeletonAll(yymax(1:round(length(yymax)/20):length(yymax),:),skelfile);
    
end   
         
         
         
         
   end     
    
   
   keyboard

%            
           
          
       
%         map_mesh_neg=reshape(prob_all_neg,[189,101*101]);
%         map_mesh_neg=reshape(max(map_mesh_neg),[101,101]);
%         
%         map_mesh_pos=reshape(prob_all_pos,[189,101*101]);
%         map_mesh_pos=reshape(max(map_mesh_pos),[101,101]);
%         
        map_mesh_oc=reshape(oc_sum,[189,101*101]);
        map_mesh_oc=reshape(max(map_mesh_oc),[101,101]);
%         
        map_like_pos=reshape(like_pos,[189,101*101]);
        map_like_pos=reshape(max(map_like_pos),[101,101]);
%         
        map_like_neg=reshape(like_neg,[189,101*101]);
        map_like_neg=reshape(max(map_like_neg),[101,101]);
%         
         map_diff_all=reshape(prob_diff,[189,101*101]);
         map_diff_all=reshape(max(map_diff_all),[101,101]);
        
         ground_mesh=reshape(class_lab,[189,101*101]);
         ground_mesh=reshape(max(ground_mesh),[101,101]);
        
        
         map_mesh=reshape(prob_all,[189,101*101]);
         map_mesh=reshape(sum( map_mesh),[101,101]);
         
         
         
        
         
         
         
         C=drawroom(room);

        
         
        

         
        % create new figure
         
         subplot(3,2,2) % first subplot
         image(C);
         
         hold on
         ground_ind = find(map_mesh>0);
         
         s = [101,101];
         [ground_x,ground_y] = ind2sub(s,ground_ind);      
                  
         scatter(ground_x*10,ground_y*10,50,map_mesh(ground_ind),'filled');
         title('Classification');
         
         
         
        
         
        subplot(3,2,1) % first subplot
         image(C);
         hold on
         
         ground_ind = find(ground_mesh>0);
         
         s = [101,101];
         [ground_x,ground_y] = ind2sub(s,ground_ind); 
          
       
         
        scatter(ground_x*10,ground_y*10,50,'y','filled');
         title('Ground Truth');
         
         
         
         
        
         subplot(3,2,3) % first subplot
%          image(C);
%          hold on
%          ground_ind = find (map_like_pos>0);
%         % caxis([0.5 1])
%          s = [101,101];
%          [ground_x,ground_y] = ind2sub(s,ground_ind); 
%          
%          scatter(ground_x*10,ground_y*10,50, map_like_pos(ground_ind)*10,'filled');
         contourf((map_like_pos));
         title('map max');
         
         
         
         
          subplot(2,2,4) % first subplot
%          image(C);
%            hold on
%           ground_ind = find ( map_mesh_oc>0);
% %          
%          s = [101,101];
%          [ground_x,ground_y] = ind2sub(s,ground_ind);      
%          
%        %  contourf(1:10:1010,1:10:1010,map_like_neg);
%          
%         scatter(ground_x*10,ground_y*10,50, map_mesh_oc(ground_ind),'filled');
         contourf((map_like_neg))
         title('Like neg');
         
         
         subplot(3,2,5) % first subplot
%         subplot(2,2,4) % first subplot
%          image(C);
%            hold on
%           ground_ind = find ( map_mesh_oc>0);
% %          
%          s = [101,101];
%          [ground_x,ground_y] = ind2sub(s,ground_ind);      
%          
%        %  contourf(1:10:1010,1:10:1010,map_like_neg);
%          
%         scatter(ground_x*10,ground_y*10,50, map_mesh_oc(ground_ind),'filled');
         contourf((map_diff_all))
         title('Like Diff');
        
         
         
         
         
%          subplot(2,3,4) % first subplot
%          image(C);
%          hold on
%          scatter(ground_x*10,ground_y*10,50,map_mesh(ground_ind),'filled');
%          title('occupied');
%          
%          subplot(4,2,5) % first subplot
%          image(C);
%          hold on
%          scatter(ground_x*10,ground_y*10,50,map_mesh(ground_ind),'filled');
%          title('like pos');
%          
%          subplot(4,2,6) % first subplot
%          contourf(map_like_neg);
%          title('like neg');
%          
%          subplot(4,2,7) % first subplot
%         image(C);
%          hold on
%          scatter(ground_x*10,ground_y*10,50,map_mesh(ground_ind),'filled');
%          title('like diff');
%          
%          subplot(4,2,8) % first subplot
%          image(C);
%          hold on
%          scatter(ground_x*10,ground_y*10,50,map_mesh(ground_ind),'filled');
%          title('feature');
%          
%          
         plot_file=sprintf('./Dataset/room%d/%s/plot.fig',room,skel_file{affordance{affordance_type}(pose)}); 
         savefig(plot_file);
         
        
         
   %   system('pkill -u jmpiyath')
    
         
    
    
             
            
            skel_mat=sprintf('./full_mat/skel_points_%s.mat',skel_file{affordance{affordance_type}(pose)});   

           load(skel_mat,'full_mat');

            Torso_mat =full_mat(3:P_length:length(full_mat),:);
                    
           full_mat=[];
           


          mapfile = sprintf('./Dataset/room%d/map.mat',room);  
        
          load(mapfile,'map');   
                  
          center=map.center;

          Zheight =meanAll(3)/10-center(3)+50-1;


        
       
        Torso_mat =Torso_mat+repmat([0, 0, Zheight,0 ],length(Torso_mat),1);   
        
%         map_mesh=reshape(prob_all,[189,101*101]);
%         map_mesh=reshape(sum(map_mesh),[101,101]); 
            
            
           
            
     [ B,I]= sort(prob_diff,'descend');
            
            
     I=I(1:100);      
 
 LLL=length(I)
 
 %keyboard
 Sit= Torso_mat(I',:);
Sit_tra= (Sit(:,1:3)+repmat([-50+center(1) -50+center(2) -50+center(3)],size(Sit,1),1))*10;
 Sit_tra= [Sit_tra,Sit(:,4)] ;   

            
            
            
            
              
 yymax=Sit_tra;

 skel_mat_points=sprintf('./Dataset/room%d/%s/skel_points.mat',room,skel_file{affordance{affordance_type}(pose)});   

 Vskel2=load(skel_mat_points);


 Vskel3 =Vskel2.Vskel1;

 
 
 
 
global Vskel;
Vskel=Vskel3;
global mapresolution;
global skelresolution;
mapresolution=1;
skelresolution=1;
%x=[ 564.7350  187.4020  231.0440    5.1000] % ground truth

%xnew =[483.5000  219.6990  231.0440    4.1000];
%drawskeleton(xnew);
length(yymax)

skelfile=sprintf('./Dataset/room%d/%s/infer.pcd',room,skel_file{affordance{affordance_type}(pose)});   

if length(yymax)<20
drawskeletonAll(yymax,skelfile);


else
drawskeletonAll(yymax(1:round(length(yymax)/20):length(yymax),:),skelfile);
    
end   
              
         II= prob_all';
         
         a= sum(class_lab .* II);
         c=  sum(~class_lab .* II);
         b= sum(class_lab .* ~II);
         d= sum(~class_lab .*~II);
         
         Fscore= 2*a/(2*a+b+c)
         
         
         performance =[a,b,c,d, a/(a+b),b/(a+b) Fscore];
         
         perfor_file=sprintf('./Dataset/room%d/%s/perfor.mat',room,skel_file{affordance{affordance_type}(pose)});   
         
         
         save( perfor_file,'performance'); 
              
         
                
        
     
        
        
          
             
    

        
        
         
         
        
  
  
  
  
  
    

  
  
 
  
 
  

      
      
      
      







  
  
  
  
