skel_file =cellstr(['relaxing';'leaning ';'working ']);
room =[1,8,7];
x=  [0 : 10];
    
y=zeros(3,11);

room_array{1}=[1,11];
room_array{2}=[5,6,8,9,10];
room_array{3}=[3,4,5,6,7];

% room_array{1}=[1,11];
% room_array{2}=[8,6];
% room_array{3}=[7,6,3,4];
% 


figure

for pose=1:3
    
  for rr=1: length(room_array{pose})
      
  room= room_array{pose}(rr);
    
  file_name= sprintf('./Dataset/room%d/%s/svm_imbalance.mat',room,skel_file{(pose)});
  
  load(file_name,'imbalance');  
    
  y(pose,:)=  y(pose,:)+imbalance.Fscore(1:11)/length(room_array{pose});
  
  end
    
end

 plot(x,y(1,:),'g',x,y(2,:),'b--o',x,y(3,:),'r--+','LineWidth',2,'MarkerSize',10);
grid on  
        len=legend('Sitting Relaxing','Standing Working','Sitting Working')
        set(len,'FontSize',12,'Location','northeast');
       % title('Ground Truth','FontSize',12);
       ylabel('F1 Score','FontSize',12)
       xlabel('log2(Imbalance Ratio)','FontSize',12) 
        
        title('Class Imbalance Test','FontSize',12);
        set(gcf, 'PaperUnits', 'inches');
        set(gcf, 'PaperSize', [8.00 6.00]);
        set(gcf, 'PaperPositionMode', 'manual');
       set(gcf, 'PaperPosition', [0 0 8.00 6.00]);
        
         
       file_name= sprintf('imbalance_test.pdf');
       print(gcf, '-dpdf', file_name); 