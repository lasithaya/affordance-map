function [ value2]  = getSkelFeatures2( x,SkelG1,DM,center,mapresolution)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here






xx=x(1)/10;
yy=x(2)/10;
zz=x(3)/10;
angle =x(4);
rotmat=rotation(angle);
W=x(5);

map_offset =50*mapresolution;
SkelG2 = SkelG1*mapresolution/10;

% new tranformed position


New_skel1 =(rotmat*SkelG2)'+repmat([xx+map_offset-center(1,1) yy+map_offset-center(1,2) zz+map_offset-center(1,3) ],length(SkelG2'),1);

 
value2 = DM(New_skel1);



% Occupancy check





end

