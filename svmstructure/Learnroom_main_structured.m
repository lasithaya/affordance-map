


function [model]= Learnroom_main_structured
skel_file =cellstr(['relaxing';'leaning ';'working ']);
global pose;
pose=2;
format long;
global room;
global model_vec;
global itter 
 global itter_f1
global f1_vec;  
% room_array{1}=[1,2,11];
% room_array{2}=[5,6,8,9,10];
% %room_array{2}=[10];
% 
% room_array{3}=[3,4,5,6,7];

room_array{1}=[2];
room_array{2}=[8];
%room_array{2}=[10];

room_array{3}=[7];





for pose= 3:3


    for rr=1:length( room_array{pose})
        
      room=  room_array{pose}(rr) 
    


         itter_f1=1;
         f1_vec=[];


      
       model_vec=[];
       
       itter=1;
          
          
             
              
         
            % groundlabel_mat=sprintf('./Dataset/room%d/%s/class_lab.mat',ff(n),skel_file{affordance{affordance_type}(pose)});   
            % load(groundlabel_mat,'class_lab');
            
              
    
    
             label_name=sprintf('./Dataset/room%d/%s/class_label_all.mat',room,skel_file{pose}) ;% load file   
             load( label_name,'class_label_all');
            
         
             lab = class_label_all';
                                
          
             labels{1} =lab;       
                    
         
             feature_name=sprintf('./Dataset/room%d/%s/features_all.mat',room,skel_file{pose}) ;% load file 
         
             patterns{1}= feature_name;
          
           

           
    
     parm.patterns = patterns ;
     parm.labels = labels ;
     parm.lossFn = @lossCB ;
     parm.constraintFn  = @constraintCB ;
     parm.featureFn = @featureCB ;
    % parm.endIterationFn = @iterCB ;
     parm.dimension = 2000 ;
     parm.verbose = 1 ;
     model = svm_struct_learn(' -c 100  -v 1 -e 10', parm) ;
 
          
%     model_name=sprintf('./Dataset/room%d/%s/model_struct.mat',room,skel_file{pose}) ;% load file   
%     save(model_name,'model');
    
     
 
    end 
   
   % system('pkill -u jmpiyath');
    
end   
end  

function cont =iterCB(param,model)


cont= false


    

end

 function psi = featureCB(param, x, y)
     
     
    fprintf('feature function \n')  
  
    features=load(x,'features');
    features=features.features;
  
   dim=size( features,1);


  
    features=(features.*repmat(int8(y'),dim,1));
  

     psi =(sparse( double(sum((features),2)))) ;
    
     

    fprintf('********************************************\n')  
    

 end        
        
 
 function delta = lossCB(param, y, ybar)
  global pose
  global room
  a= sum((y+ybar)==2);
  b = sum((y==-1) .* (ybar==1));
  c= sum((y==1) .* (ybar==-1));
   
   
  delta =100*(1-(2*a/(2*a+b+c)))
  
 skel_file =cellstr(['relaxing';'leaning ';'working ']); 
 F1=(2*a/(2*a+b+c));
global itter_f1
global f1_vec;
itter_f1=itter_f1+1;
f1_vec(itter_f1)=F1;
f1_name=sprintf('./Dataset/room%d/%s/f1_vec.mat',room,skel_file{pose}) ;% load file   
save(f1_name,'f1_vec');
  
   room
 
  end
        
 function yhat = constraintCB(param, model, x, y)
   tic; 
   global room
   global pose
   
  fprintf('constrain function\n')    
      
 
    
    
  %  W=rand(1000,1);
  
  
  W=model.w;
  features=load(x,'features');
  features=features.features;
    
    
    
    
  wphi_pos=double(W'*single(features));
  clearvars 'features';
   


  
   
  Pos = find(y==1);
  Neg =find(y==-1);
  
  posW= wphi_pos(Pos);
 
  pos_sum_ALLN=-1*sum(posW);
  
  [posd,Ipos]=sort(posW,'descend');
  
  sorted_pos =Pos(Ipos);
 
  
  negW= wphi_pos(Neg);
  neg_sum = sum(negW);
  
  [pNegd,Ineg]=sort(negW,'descend');
  
 
  
  
  sorted_neg =Neg(Ineg);
 % sorted_neg =[sorted_neg;1];
  
  pos_num=length(Pos)
  
  neg_num =length(Neg)
  

 %prev_max_con=-inf;
 
 max_con_pos_max= (zeros((pos_num+1), 1));
 max_con_pos_a= (zeros((pos_num+1), 1));
 max_con_pos_b= (zeros((pos_num+1), 1));
 
 %max_con_neg= (zeros((neg_num+1) ,3));
  %II_pos=0;
  
 % sorted_neg_rev= sorted_neg(length(sorted_neg):-1:1);  
  w_neg=pNegd((length(pNegd):-1:1));
  w_neg=[0,w_neg];
  w_neg_sum=(cumsum(w_neg));
  
  w_pos =[0,posd];
  w_pos_sum=cumsum(w_pos);

  
   parfor a=0:pos_num
   
   c=pos_num-a;
  % pos_sum =pos_sum+abs(2*wphi_pos(sorted_pos(a+1)));
  % Y_star(sorted_pos(a+1))=1;
   pos_sum =  pos_sum_ALLN+(2*w_pos_sum(a+1));
   
  % II_neg=0;
  % loop_sum_neg = neg_sum; 
   
   oneV=ones(1,length(w_neg_sum));
   bI=(neg_num:-1:0);
 % max_neg= oneV*(pos_sum +neg_sum+100)-w_neg_sum*2-100*((2*a*oneV)./ ( (2*a+c)*oneV+bI));
   prec =oneV*100-100*((2*a*oneV)./ ( (2*a+c)*oneV+bI));
  
   neg_sum_all = oneV*neg_sum-(w_neg_sum*2);
  
   pos_sum_all =oneV*pos_sum;
   max_neg=(pos_sum_all+neg_sum_all)+prec;
   
 
    
   [max_con_pos_max(a+1),Ind]= max(max_neg);
   max_con_pos_a(a+1) =a;
   max_con_pos_b(a+1)= neg_num-Ind+1;
   
 
     
   end  
  
 
   toc;
    
   % keyboard;
    [MM,Ind]= max(max_con_pos_max)
    a_max= max_con_pos_a(Ind)
    b_max=max_con_pos_b(Ind)
    
     
     Y_star= ones(length(y),1)*-1;
     
     if a_max>0
     Y_star(sorted_pos(1:a_max))=1;
     end
     
     if b_max>0
     Y_star(sorted_neg(1:b_max))=1;
     end       
   
     
   yhat= Y_star;

skel_file =cellstr(['relaxing';'leaning ';'working ']);
global itter
global model_vec

model_vec{itter}=model.w
itter=itter+1;

model_name=sprintf('./Dataset/room%d/%s/model_struct.mat',room,skel_file{pose}) ;% load file   
save(model_name,'model_vec');


fprintf('********************************************\n')     
        
 end
        
        
        



  
 
 


  
  




  
 
  
  
  
  
  
  
  
  
  
  
  
  
  