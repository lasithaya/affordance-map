ff=[1,3,4,5,6,7];

k=6;
    
     
    model_name=sprintf('./Dataset/living%d/model.mat',ff(k));
    
    load(model_name,'model');   
     
    mapfile = sprintf('./Dataset/living%d/map.mat',ff(k));  
        
    load(mapfile,'map');   
 
 
    YG_name=sprintf('./Dataset/living%d/YGround.mat',ff(k));  % load groundtruth
    
    load(YG_name,'ymax'); % load ground truth
     
    y = ymax(1,:);

Vskel2=load('Vskel.mat');

Vskel1 =Vskel2.Vskel1;
global mapresolution;
mapresolution=1;

DM=map.dis;  
  center =map.center;
  minroom =  map.minroom;
  maxroom =  map.maxroom;
 
  Dfeat=zeros(size(y,1),15);
 for u=1:size(y,1) 
    ybar=y(u,:);
 
    Dfeat(u,:)=(getSkelFeatures(ybar,Vskel1,DM,center,mapresolution))';
 
 end

 Dfeat1 = (mean(Dfeat,1));
 Dfeat1=sparse(double(Dfeat1'));
 
 psi =getSkelFeatures(y,Vskel1,DM,center,1);
 psi = sparse(double(psi)) ;
