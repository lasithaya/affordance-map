skel_file =cellstr(['relaxing';'leaning ';'working ']);
%global Vskel;

for ff=1:8

for poses=1:3

Vskel_file=sprintf('./Dataset/room%d/%s/Vskel.mat',ff,skel_file{poses}); 
skel_mat=sprintf('./Dataset/room%d/%s/skel_points.mat',ff,skel_file{poses});   
    


mapresolution =1;
skelresolution=1;

Vskel2=load(Vskel_file);

Vskel =Vskel2.Vskel1;

x=[0,0,0,0];

resofactor= (mapresolution*skelresolution);

hold on;
xx=(x(1));
yy=(x(2));
zz=(x(3));
angle =x(4);
Pcloud =[];
rotmat=rotation(angle);
Neck =rotmat* Vskel(:,2)*resofactor +[xx;yy;zz] ;
Head=rotmat* Vskel(:,1)*resofactor +[xx;yy;zz] ;
Left_shoulder=rotmat* Vskel(:,4)*resofactor +[xx;yy;zz] ;
Left_elbow=rotmat* Vskel(:,5)*resofactor +[xx;yy;zz] ;
Left_hand=rotmat* Vskel(:,12)*resofactor +[xx;yy;zz] ;
Right_shoulder=rotmat* Vskel(:,6)*resofactor +[xx;yy;zz] ;
Right_elbow=rotmat* Vskel(:,7)*resofactor +[xx;yy;zz] ;
Right_hand=rotmat* Vskel(:,13)*resofactor +[xx;yy;zz] ;
Left_hip =rotmat* Vskel(:,8)*resofactor +[xx;yy;zz] ;
Left_knee=rotmat* Vskel(:,9)*resofactor +[xx;yy;zz] ;
Left_foot=rotmat* Vskel(:,14)*resofactor +[xx;yy;zz] ;
Right_hip=rotmat* Vskel(:,10)*resofactor +[xx;yy;zz] ;
Right_knee= rotmat* Vskel(:,11)*resofactor +[xx;yy;zz] ;
Right_foot=rotmat* Vskel(:,15)*resofactor +[xx;yy;zz] ;
Torso =rotmat* Vskel(:,3)*resofactor +[xx;yy;zz] ;

Pcloud =[Pcloud; [Head';Neck';Torso';Left_shoulder';Left_elbow';Left_hand';Right_shoulder';Right_elbow';Right_hand';Left_hip';Left_knee';Left_foot';Right_hip';Right_knee';Right_foot']];


% Pcloud=[Pcloud;drawline2(Head,Neck,3)];
% Pcloud=[Pcloud;drawline2(Neck,Left_shoulder,5)];
% Pcloud=[Pcloud;drawline2(Left_shoulder,Left_elbow,5)];
% Pcloud=[Pcloud;drawline2(Left_elbow,Left_hand,5)];
% 
% Pcloud=[Pcloud;drawline2(Left_shoulder,Left_hip,5)];
% Pcloud=[Pcloud;drawline2(Right_shoulder,Right_hip,5)];
% 
% Pcloud=[Pcloud;drawline2(Left_knee,Right_knee,5)];
% 
% Pcloud=[Pcloud;drawline2(Left_hip,Right_knee,5)];
% Pcloud=[Pcloud;drawline2(Right_hip,Left_knee,5)];
% 
% Pcloud=[Pcloud;drawline2(Neck,Right_shoulder,5)];
% Pcloud=[Pcloud;drawline2(Right_shoulder,Right_elbow,5)];
% Pcloud=[Pcloud;drawline2(Right_elbow,Right_hand,5)];
% 
% Pcloud=[Pcloud;drawline2(Left_shoulder,Torso,5)];
% Pcloud=[Pcloud;drawline2(Right_shoulder,Torso,5)];
% Pcloud=[Pcloud;drawline2(Left_hip,Torso,5)];
% Pcloud=[Pcloud;drawline2(Right_hip,Torso,5)];
% Pcloud=[Pcloud;drawline2(Right_hip,Left_hip,5)];
% 
% Pcloud=[Pcloud;drawline2(Left_hip,Left_knee,5)];
% Pcloud=[Pcloud;drawline2(Left_knee,Left_foot,5)];
% 
% Pcloud=[Pcloud;drawline2(Right_hip,Right_knee,5)];
% Pcloud=[Pcloud;drawline2(Right_knee,Right_foot,5)];


Vskel1 =Pcloud';

save(skel_mat,'Vskel1');


end
end