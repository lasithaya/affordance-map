skel_file =cellstr(['relaxing';'leaning ';'working ']);
room =[1,8,7];
x=  [1 : 50];
    
Fscore=zeros(3,50);

% room_array{1}=[1,11];
% room_array{2}=[5,6,8,9,10];
% room_array{3}=[5,6,3];

room_array{1}=[1,11];
room_array{2}=[5,6,8,9,10];
room_array{3}=[3,4,5,6,7];


room_array{1}=[2];
room_array{2}=[8];
room_array{3}=[7];



%figure

for pose=1:3
    
  for rr=1: length(room_array{pose})
      
  room= room_array{pose}(rr);
    
  f1_name=sprintf('./Dataset/room%d/%s/f1_vec.mat',room,skel_file{pose}) ;% load file   
load(f1_name,'f1_vec');
    
 Fscore(pose,:)= f1_vec(1:50);

  end
    
end
Fscore
Pre
Rec


 plot(x,Fscore(1,:),'g',x,Fscore(2,:),'b--o',x,Fscore(3,:),'r--+','LineWidth',2,'MarkerSize',10);
grid on  
        len=legend('Sitting Relaxing','Standing Working','Sitting Working')
        set(len,'FontSize',12,'Location','northwest');
       % title('Ground Truth','FontSize',12);
       ylabel('F1 Score','FontSize',12)
       xlabel('Iterations','FontSize',12) 
        
        title('The Behavior of F1-score in S-SVM Training Process','FontSize',12);
        set(gcf, 'PaperUnits', 'inches');
        set(gcf, 'PaperSize', [8.00 6.00]);
        set(gcf, 'PaperPositionMode', 'manual');
       set(gcf, 'PaperPosition', [0 0 8.00 6.00]);
        
         
       file_name= sprintf('f1_test.pdf');
       print(gcf, '-dpdf', file_name); 