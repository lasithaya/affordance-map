function skelcloud=drawskeletonAll(Ymax,skelfile)
fprintf('generating skeletons \n')

global Vskel;
resofactor= 1;
skelresolution=10;
mapresolution=1;
D=[];
for i=1:size(Ymax,1)
    
x=Ymax(i,:);
hold on;
xx=(x(1));
yy=(x(2));
zz=(x(3));
angle =x(4);
Pcloud =[];
rotmat=rotation(angle);
Neck =rotmat* Vskel(:,2)*resofactor +[xx;yy;zz] ;
Head=rotmat* Vskel(:,1)*resofactor +[xx;yy;zz] ;
Left_shoulder=rotmat* Vskel(:,4)*resofactor +[xx;yy;zz] ;
Left_elbow=rotmat* Vskel(:,5)*resofactor +[xx;yy;zz] ;
Left_hand=rotmat* Vskel(:,12)*resofactor +[xx;yy;zz] ;
Right_shoulder=rotmat* Vskel(:,6)*resofactor +[xx;yy;zz] ;
Right_elbow=rotmat* Vskel(:,7)*resofactor +[xx;yy;zz] ;
Right_hand=rotmat* Vskel(:,13)*resofactor +[xx;yy;zz] ;
Left_hip =rotmat* Vskel(:,8)*resofactor +[xx;yy;zz] ;
Left_knee=rotmat* Vskel(:,9)*resofactor +[xx;yy;zz] ;
Left_foot=rotmat* Vskel(:,14)*resofactor +[xx;yy;zz] ;
Right_hip=rotmat* Vskel(:,10)*resofactor +[xx;yy;zz] ;
Right_knee= rotmat* Vskel(:,11)*resofactor +[xx;yy;zz] ;
Right_foot=rotmat* Vskel(:,15)*resofactor +[xx;yy;zz] ;
Torso =rotmat* Vskel(:,3)*resofactor +[xx;yy;zz] ;

Pcloud=[Pcloud;drawline(Head,Neck,80)];
Pcloud=[Pcloud;drawline(Neck,Left_shoulder,10)];
Pcloud=[Pcloud;drawline(Left_shoulder,Left_elbow,10)];
Pcloud=[Pcloud;drawline(Left_elbow,Left_hand,10)];

Pcloud=[Pcloud;drawline(Neck,Right_shoulder,10)];
Pcloud=[Pcloud;drawline(Right_shoulder,Right_elbow,10)];
Pcloud=[Pcloud;drawline(Right_elbow,Right_hand,10)];

Pcloud=[Pcloud;drawline(Left_shoulder,Torso,10)];
Pcloud=[Pcloud;drawline(Right_shoulder,Torso,10)];
Pcloud=[Pcloud;drawline(Left_hip,Torso,10)];
Pcloud=[Pcloud;drawline(Right_hip,Torso,10)];
Pcloud=[Pcloud;drawline(Right_hip,Left_hip,10)];

Pcloud=[Pcloud;drawline(Left_hip,Left_knee,10)];
Pcloud=[Pcloud;drawline(Left_knee,Left_foot,10)];

Pcloud=[Pcloud;drawline(Right_hip,Right_knee,10)];
Pcloud=[Pcloud;drawline(Right_knee,Right_foot,10)];
hold off

[X1,Y1,Z1] = (sphere);
X=X1/ 2;
Y=Y1/2;
Z=Z1/2;

Pcloud=[Pcloud;(repmat(Head',length(X(:)),1))+[X(:),Y(:),Z(:)]*0.5*skelresolution];

Pcloud=[Pcloud;(repmat(Neck',length(X(:)),1))+[X(:),Y(:),Z(:)]*0.2*skelresolution];

Pcloud=[Pcloud;(repmat(Left_shoulder',length(X(:)),1))+[X(:),Y(:),Z(:)]*0.2*skelresolution];

Pcloud=[Pcloud;(repmat(Left_elbow',length(X(:)),1))+[X(:),Y(:),Z(:)]*0.2*skelresolution];

Pcloud=[Pcloud;(repmat(Left_hand',length(X(:)),1))+[X(:),Y(:),Z(:)]*0.2*skelresolution];

Pcloud=[Pcloud;(repmat(Right_shoulder',length(X(:)),1))+[X(:),Y(:),Z(:)]*0.2*skelresolution];

Pcloud=[Pcloud;(repmat(Right_elbow',length(X(:)),1))+[X(:),Y(:),Z(:)]*0.2*skelresolution];

Pcloud=[Pcloud;(repmat(Right_hand',length(X(:)),1))+[X(:),Y(:),Z(:)]*0.2*skelresolution];

Pcloud=[Pcloud;(repmat(Left_hip',length(X(:)),1))+[X(:),Y(:),Z(:)]*0.2*skelresolution];

Pcloud=[Pcloud;(repmat(Left_knee',length(X(:)),1))+[X(:),Y(:),Z(:)]*0.2*skelresolution];

Pcloud=[Pcloud;(repmat(Left_foot',length(X(:)),1))+[X(:),Y(:),Z(:)]*0.2*skelresolution];

Pcloud=[Pcloud;(repmat(Right_hip',length(X(:)),1))+[X(:),Y(:),Z(:)]*0.2*skelresolution];

Pcloud=[Pcloud;(repmat(Right_knee',length(X(:)),1))+[X(:),Y(:),Z(:)]*0.2*skelresolution];

Pcloud=[Pcloud;(repmat(Right_foot',length(X(:)),1))+[X(:),Y(:),Z(:)]*0.2*skelresolution];

Pcloud =[Pcloud; [Torso';Neck';Head';Left_shoulder';Left_elbow';Left_hand';Right_shoulder';Right_elbow';Right_hand';Left_hip';Left_knee';Left_foot';Right_hip';Right_knee';Right_foot']];
D=[D;Pcloud*mapresolution];



end

R =repmat([1 0 0],length(D),1);

skelcloud= [D,R]';
savepcd(skelfile,[D,R]');

end



